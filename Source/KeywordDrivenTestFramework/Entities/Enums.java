package KeywordDrivenTestFramework.Entities;

import KeywordDrivenTestFramework.Utilities.ApplicationConfig;

public class Enums {

    static String reportDirectory = ApplicationConfig.ReportFileDirectory();
    public enum BrowserType {

        IE, FireFox, Chrome, Safari
    }

    public enum ResultStatus {

        PASS, FAIL, WARNING, UNCERTAIN
    }

    public enum RelativePosition {

        Above, Below, Right, Left
    }

    public enum Environment {
     
        AddUserListURL("http://www.way2automation.com/angularjs-protractor/webtables/","http://www.way2automation.com/angularjs-protractor/webtables/");
    
        // For each system (website1, database1, website2 etc.) within the defined environment (Dev, QA, Prod etc.)
        // you will have to declare the appropriate string to store its properties (URL or connection string etc.).
        public final String PageUrl;
        public final String Ip;

//        public final String ForgotPasswordURL;
        // This constructor defines and instantiates the parameters declared above. Parameter order is specified here and will 
        // define the order in which the enum types' properties are specified. 
        Environment(String pageUrl,String ip) {
            this.PageUrl = pageUrl;
            this.Ip = ip;
        }

    }

    public static Environment resolveTestEnvironment(String environment) {
        switch (environment.toUpperCase()) {

            case "ADDUSERLISTURL":
                return Environment.AddUserListURL;
            default:
                return null;
        }
    }
}
