package KeywordDrivenTestFramework.Reporting;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.reportDirectory;
import KeywordDrivenTestFramework.Entities.Enums;
import KeywordDrivenTestFramework.Entities.TestEntity;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.TestMarshall;
import static KeywordDrivenTestFramework.Testing.TestMarshall.regressionDateTime;
import KeywordDrivenTestFramework.Utilities.ApplicationConfig;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;
import com.relevantcodes.extentreports.NetworkMode;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import org.apache.commons.codec.binary.Base64;
import jxl.Workbook;
import jxl.write.Label;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import java.io.File;
import java.io.IOException;
import java.util.Calendar;
import jxl.Sheet;
import jxl.format.Colour;
import jxl.format.UnderlineStyle;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;


public class Narrator extends BaseClass
{

    private TestEntity testData;
    private static final String formatStr = "%n%-24s %-20s %-60s %-25s";
    private static String logMessage = "";
    private static int counter = 0;
    private static int failRowCounter = 1;
    private static int stepCounter = 0;
    //private long totalTime = 0;
    private static final DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss:ms");
    //private static Date startDate;
    private static ExtentReports extentReports;
    private static ExtentTest extentTest;
    public static WritableWorkbook workbook;
    public static Workbook workbook2;
    public static Sheet sheet2;
    public static WritableSheet sheet;
    public static ArrayList <String> CollectionFailedItemsList = new ArrayList();
    public static ArrayList <String> CollectionFailedMessageList = new ArrayList();
    public static ArrayList <String> CollectionFailedPackList = new ArrayList();
    public static ArrayList <Colour> CollectionColourPackList = new ArrayList();
    public static String Testpack;
    public static int colourCount = 0;
    public static Calendar calendar = Calendar.getInstance();
    public static SimpleDateFormat dateFormatter = new SimpleDateFormat("(yyyy-MM-dd)(HH-mm-ss)");
    public static  String  dateTimeFolder2 = dateFormatter.format(calendar.getTime());
//    private String ExtentLocation;
//    private String ExtentWrite;
    
    Enums.BrowserType browser = ApplicationConfig.SelectedBrowser();
    
    
    String directory;

    //Initializes the Narrator class
    public Narrator(TestEntity testData)
    {
        this.testData = testData;
        this.setStartTime();
        directory = reportDirectory + "\\Narrator_Log.txt";
        File file = new File(directory);
        extentReports = new ExtentReports(reportDirectory + "\\extentReport.html", false, NetworkMode.OFFLINE);  //Set to false so tests are appended
        extentReports.config()
                .documentTitle("DVT Automation Report")
                .reportName("DVT Report");
        String style = ".test { border: 2px solid #444; }";
        extentReports.config().insertCustomStyles(style);
        extentTest = extentReports.startTest("<span style='font-weight:bold;font-family: Georgia;font-size: 14px'>" + testData.TestCaseId + "</span>",
                "<span style='font-weight:bold;font-family: Georgia;font-size: 30px'>" + testData.TestDescription + "</span></br>"
                        + "<span style='font-weight:bold;font-family: Georgia;font-size: 20px;float: left;'>Browser Type: " + browser + "</span></br><p></p>"
                        + "<span style='font-weight:bold;font-family: Georgia;font-size: 20px;float: left;'>Environment: " + TestMarshall.currentEnvironment.PageUrl + "</span></br><p></p>"
                        + "<span style='font-weight:bold;font-family: Georgia;font-size: 20px;float: left;'>Test: " + testData.TestMethod + "</span>");
                        dashBoardUtil.addTestCaseIdArray(testData.TestCaseId);
                        dashBoardUtil.addTestArray(testData.TestMethod);
                        
        //checks if file exists if not, create it
        if (!file.exists()) {
            createNewTextFile();
        }

        try {
            //initializes the text file with new test class data     
            counter = 0;
            PrintWriter writer = new PrintWriter(new FileWriter(file, true));
            //writer.println(" ");
            writer.println(String.format(formatStr, dateFormat.format(new Date()), "- [KEYS] START KEYWORD:", testData.TestCaseId, ""));
            writer.close();
//            workbook.close();
        } 
//        catch (WriteException e) 
//        {
//            e.printStackTrace();
//            
//        }
        catch (IOException e) 
        {
            System.out.printf(e.getMessage());
        }
        
        
    }

    //Takes the screenshot
    public static void takeScreenShot(boolean testStatus, String message) 
    {
        if(SeleniumDriverInstance.isDriverRunning())
            SeleniumDriverInstance.takeScreenShot(++counter + " " + message, !testStatus);
        else if(SikuliDriverInstance.isDriverRunning())
            SikuliDriverInstance.TakeScreenshot(++counter + " " + message, testStatus);
    }

    //Used when a test passes
    //Writes to the text file and writes the html file
    public void stepPassed(String message, boolean homepage) 
    {
        logMessage = checkMessageLen(message);
        takeScreenShot(true, logMessage);
        
        extentTest.log(LogStatus.PASS, "<span style='font-weight:bold;font-family: Georgia; '>" + message + "</span>");
        writeToTextFile(message, "Step Passed");
        //Writes info to the text file  
        logInfo("[INFO] STEP " + ++stepCounter + ":" + logMessage);
    }
    
    public void stepPassedWithScreenshot(String message, boolean homepage) 
    {
        
        logMessage = checkMessageLen(message);
        takeScreenShot(true, logMessage);
        
        String embeddedImage = convertPNGToBase64(getScreenshotPath());
        extentTest.log(LogStatus.PASS, "<span style='font-weight:bold;font-family: Georgia; '>" + message + "</span>"+ extentTest.addScreenCapture("data:image/png;base64," + embeddedImage));
        writeToTextFile(message, "Step Passed With Screenshot");
        //Writes info to the text file  
        logInfo("[INFO] STEP " + ++stepCounter + ":" + logMessage);
    }
    
    public void stepMessage(String message) 
    {
        
        logMessage = checkMessageLen(message);
        takeScreenShot(true, logMessage);
        extentTest.log(LogStatus.PASS, "<span style='font-weight:bold;font-family: Georgia; '>" + message + "</span>");
        writeToTextFile(message, "Step Message");
        //Writes info to the text file  
        logInfo("[INFO] STEP " + ++stepCounter + ":" + logMessage);      
    }
    
    //Used where a test fails
    //Writes to the  text file and writes to the html file
    public TestResult testFailed(String message, boolean toMainPage) 
    {
        int tempTotalFails = dashBoardUtil.getTotalFailures();
        dashBoardUtil.setTotalFailures(tempTotalFails+1);
        dashBoardUtil.addStatusArray("FAIL");
        
        //takes sceenshot
        takeScreenShot(false, logMessage);
        
        logMessage = checkMessageLen(message);
        
        String embeddedImage = convertPNGToBase64(getScreenshotPath());
        boolean j = TestMarshall.jenkins;
//        if (j == false)
//        {
            extentTest.log(LogStatus.FAIL,"<span style='font-weight:bold;font-family: Georgia;'>" + message + "</span>" + extentTest.addScreenCapture("data:image/png;base64," + embeddedImage));
            writeToTextFile(message, "Test Failed");
            extractedParameters(testData);
            extentReports.endTest(extentTest);
            extentReports.flush();
            
            
            CollectionFailedMessageList.add(message);
            CollectionFailedItemsList.add(testData.TestCaseId);
            CollectionFailedPackList.add(Testpack);
            
//            writeToExcel(testData.TestCaseId, message, "FAIL");
//            failRowCounter++;
//        }
//        else if (j = true)
//        {
//            if (!testData.TestCaseId.startsWith("#")) 
//            {
//                extentTest.log(LogStatus.FAIL,"<span style='font-weight:bold;font-family: Georgia;'>" + message + "</span>" + extentTest.addScreenCapture("data:image/png;base64," + embeddedImage));
//                writeToTextFile(message, "Test Failed");
//                extractedParameters(testData);
//                extentReports.endTest(extentTest);
//                extentReports.flush();
//            }
//        }
        //Writes info to the text file

        logFailure("[ERR] STEP " + ++stepCounter + ":" + logMessage);
        
        TestResult testResult = new TestResult(testData, Enums.ResultStatus.FAIL, message, this.getTotalExecutionTime());
//        j=true;
        if(j)
        {
            CSVBatchReporter genReport = new CSVBatchReporter(""+TestMarshall.regressionDateTime,System.getProperty("user.dir")+"\\CSVReports\\RegressionReports\\",this.testData ,testResult, this.getTotalExecutionTime(), TestMarshall.currentEnvironment.PageUrl);
            genReport.createRegressionReportCSV();
        }
        
        return new TestResult(testData, Enums.ResultStatus.FAIL, message, this.getTotalExecutionTime());
    }

    //Use at the end of a test class when everything passes
    //Writes to the text file and writes the html file
    public TestResult finalizeTest(String message, boolean toMainPage, TestEntity testData) 
    {
        int tempTotalPasses = dashBoardUtil.getTotalPasses();
        dashBoardUtil.setTotalPasses(tempTotalPasses+1);
        dashBoardUtil.addStatusArray("PASS");
        
        logMessage = checkMessageLen(message);
        
        //takes sceenshot
        takeScreenShot(true, logMessage);
        
        String embeddedImage = convertPNGToBase64(getScreenshotPath());
        boolean j = TestMarshall.jenkins;
//        if (j == false)
//        {
            extentTest.log(LogStatus.PASS, "<span style='font-weight:bold;font-family: Georgia; '>" + message + "</span>" + extentTest.addScreenCapture("data:image/png;base64," + embeddedImage));
            extractedParameters(testData);
            testDataDisplay(testData);
            extentReports.endTest(extentTest);
            extentReports.flush();
//        }
//        else if (j = true)
//        {
//            if (!testData.TestCaseId.startsWith("#")) 
//            {
//                extentTest.log(LogStatus.PASS, "<span style='font-weight:bold;font-family: Georgia; '>" + message + "</span>" + extentTest.addScreenCapture("data:image/png;base64," + embeddedImage));
//                extractedParameters(testData);
//                testDataDisplay(testData);
//                extentReports.endTest(extentTest);
//                extentReports.flush();
//            }
//        }



        writeToTextFile(message, "Finalize Test");

        //Writes info to the text file
        logPass("[INFO] TEST " + testData.TestCaseId + " PASSED:" + logMessage);
        
//        this.AppendHTML();

        TestResult testResult = new TestResult(testData, Enums.ResultStatus.PASS, message, this.getTotalExecutionTime());
//        j=true;
        if(j)
        {
            CSVBatchReporter genReport = new CSVBatchReporter(""+TestMarshall.regressionDateTime,System.getProperty("user.dir")+"\\CSVReports\\RegressionReports\\",this.testData ,testResult, this.getTotalExecutionTime(), TestMarshall.currentEnvironment.PageUrl);
            genReport.createRegressionReportCSV();
        }
         
        return testResult;
    }
    
    public void extractedParameters(TestEntity testData) 
    {
        ArrayList keys = new ArrayList();
        ArrayList values = new ArrayList();
        ArrayList status = new ArrayList();
        if (testData.ExtractedParameters != null) 
        {
            logMessage = "Extracted Parameters:";

            String extractedParameters = "<span style='font-weight:bold;font-family: Georgia;'>" + logMessage + "</span></br><table>";
              
            for (String key : testData.ExtractedParameters.keySet())
            {
                keys.add(key);
                for(String value : testData.ExtractedParameters.get(key))
                {
                    status.add(testData.ExtractedParameters.get(key).get(1));
                    values.add(value);
                    break;
                }
            }
            
            for (int i = 0; i < keys.size(); i++) 
            {
                if (status.get(i).equals("PASS")) 
                {
                    extractedParameters+= "<tr style='background: #60A84D;'><td>"+keys.get(i)+"</td><td>"+values.get(i)+"</td></tr>";
                }
                else if(status.get(i).equals("FAIL"))
                {
                    extractedParameters+= "<tr style='background: #FF4536;'><td>"+keys.get(i)+"</td><td>"+values.get(i)+"</td></tr>";
                }
                else if(status.get(i).equals("WARNING"))
                {
                    extractedParameters+= "<tr style='background: #FF8E1A;'><td>"+keys.get(i)+"</td><td>"+values.get(i)+"</td></tr>";
                }
                else
                {
                    extractedParameters+= "<tr><td>"+keys.get(i)+"</td><td>"+values.get(i)+"</td></tr>";
                }
            }

            extractedParameters+= "</table>";
            
            extentTest.log(LogStatus.PASS, extractedParameters);
        }
    }
    
    public void testDataDisplay(TestEntity testData) 
    {
        ArrayList keys = new ArrayList();
        ArrayList values = new ArrayList();
        if (testData.TestParameters != null) 
        {
            logMessage = "Test Parameters:";

            String testParameters = "<span style='font-weight:bold;font-family: Georgia;'>" + logMessage + "</span></br><table>";
              
            for (String key : testData.TestParameters.keySet())
            {
                keys.add(key);
                values.add(testData.getData(key));
            }
            
            for (int i = 0; i < keys.size(); i++) 
            {
                testParameters += "<tr><td>"+keys.get(i)+"</td><td>"+values.get(i)+"</td></tr>";
            }

            testParameters += "</table>";
            
            extentTest.log(LogStatus.PASS, testParameters);
        }
    }

    //Creating a new text file
    public static void createNewTextFile() 
    {
        //Creates the file and initializes the header
        try 
        {
            String directory = reportDirectory + "\\Narrator_Log.txt";
            File file = new File(directory);
            file.createNewFile();
            PrintWriter writer = new PrintWriter(new FileWriter(file, true));
            writer.println(String.format(formatStr, "", "-- NARRATOR LOG FILE --", "", ""));
            writer.close();
        } 
        catch (IOException e)
        {
            System.out.printf(e.getMessage());
        }
    }
    public static WritableCellFormat createFormatCellStatus(boolean b) throws WriteException
    {
        Colour colour = (b == true) ? Colour.GREEN : Colour.BLACK;
        WritableFont wfontStatus = new WritableFont(WritableFont.createFont("Arial"), WritableFont.DEFAULT_POINT_SIZE, WritableFont.BOLD, false, UnderlineStyle.NO_UNDERLINE, colour);
        WritableCellFormat fCellstatus = new WritableCellFormat(wfontStatus);
        
        
//        Colour myColour = Colour(221,221,221);
        fCellstatus.setBackground(Colour.RED);
        
        
        fCellstatus.setWrap(true);
        fCellstatus.setAlignment(jxl.format.Alignment.CENTRE);
        fCellstatus.setVerticalAlignment(jxl.format.VerticalAlignment.CENTRE);
        fCellstatus.setBorder(jxl.format.Border.ALL, jxl.format.BorderLineStyle.THIN, jxl.format.Colour.BLACK);
        return fCellstatus;
            
        
    }
    
    public static WritableCellFormat createFormatCellStatus2(boolean b, String testPackNumber) throws WriteException
    {
        Colour colour = (b == true) ? Colour.BLACK : Colour.BLACK;
        WritableFont wfontStatus = new WritableFont(WritableFont.createFont("Arial"), WritableFont.DEFAULT_POINT_SIZE, WritableFont.NO_BOLD, false, UnderlineStyle.NO_UNDERLINE, colour);
        WritableCellFormat fCellstatus = new WritableCellFormat(wfontStatus);
        
        CollectionColourPackList.add(colour.GREEN);
        CollectionColourPackList.add(colour.BLUE);
        CollectionColourPackList.add(colour.YELLOW);
        CollectionColourPackList.add(colour.RED);
        CollectionColourPackList.add(colour.AQUA);
        CollectionColourPackList.add(colour.ORANGE);
        CollectionColourPackList.add(colour.PLUM);
        CollectionColourPackList.add(colour.GOLD);
        CollectionColourPackList.add(colour.INDIGO);
        CollectionColourPackList.add(colour.PINK);
        CollectionColourPackList.add(colour.ROSE);
        
        
//        String packNumber = "Test Pack 1"; 
                    
        for (int i = 0; i < CollectionFailedPackList.size(); i++) 
        {
            if(testPackNumber.equals(CollectionFailedPackList.get(i)))
            {
                fCellstatus.setBackground(CollectionColourPackList.get(i));
            }

        }
           
        
        
//        fCellstatus.setBackground(CollectionColourPackList.get(colourCount));
        
        fCellstatus.setWrap(false);
        fCellstatus.setBorder(jxl.format.Border.ALL, jxl.format.BorderLineStyle.THIN, jxl.format.Colour.BLACK);
        return fCellstatus;
  
    }
    
    public static WritableCellFormat createFormatCellStatus4(boolean b) throws WriteException
    {
        Colour colour = (b == true) ? Colour.BLACK : Colour.RED;
        WritableFont wfontStatus = new WritableFont(WritableFont.createFont("Arial"), WritableFont.DEFAULT_POINT_SIZE, WritableFont.NO_BOLD, false, UnderlineStyle.NO_UNDERLINE, colour);
        WritableCellFormat fCellstatus = new WritableCellFormat(wfontStatus);
        
        
        fCellstatus.setWrap(false);
        fCellstatus.setBorder(jxl.format.Border.ALL, jxl.format.BorderLineStyle.THIN, jxl.format.Colour.BLACK);
        return fCellstatus;
  
    }
        public static WritableCellFormat createFormatCellStatus3(boolean b) throws WriteException
    {
        Colour colour = (b == true) ? Colour.BLACK : Colour.RED;
        WritableFont wfontStatus = new WritableFont(WritableFont.createFont("Arial"), WritableFont.DEFAULT_POINT_SIZE+1, WritableFont.BOLD, false, UnderlineStyle.NO_UNDERLINE, colour);
        WritableCellFormat fCellstatus = new WritableCellFormat(wfontStatus);
        fCellstatus.setBackground(Colour.GREY_40_PERCENT);
        
        fCellstatus.setWrap(false);
        fCellstatus.setBorder(jxl.format.Border.ALL, jxl.format.BorderLineStyle.THIN, jxl.format.Colour.BLACK);
        return fCellstatus;
  
    }
    public static void createNewExcelFile(String date_Time) 
    {
        //Creates the file and initializes the header
        try 
        {
            
            
            
            //create a new workbook to write
            workbook = Workbook.createWorkbook(new File(System.getProperty("user.dir")+"\\CSVReports\\BatchTestResults_" + date_Time + ".xls"));
            //add a new sheet to the work book
            sheet = workbook.createSheet("TestCases", 0);
            //add test Id to col 0 row 0
            
            Label testId = new Label(0, 0, "Script ID",createFormatCellStatus3(true));
            //add test name to col 1 row 0
            Label testName = new Label(1, 0, "Description",createFormatCellStatus3(true));
            //add test module to col 2 row 0
            Label testModule = new Label(2, 0, "Result",createFormatCellStatus3(true));
            //add test type to col 3 row 0
            Label testType = new Label(3, 0, "Bug ID",createFormatCellStatus3(true));
            //add test steps to col 4 row 0
            Label testSteps = new Label(4, 0, "Comment",createFormatCellStatus3(true));
            //add test actions to col 5 row 0

            //add to the cells
            sheet.addCell(testId);
            sheet.addCell(testName);
            sheet.addCell(testModule);
            sheet.addCell(testType);
            sheet.addCell(testSteps);
            
//            workbook.write();
//            workbook.close();
                       
            
        } 
        catch (WriteException e) 
        {
            e.printStackTrace();
            
        }
        catch(IOException err)
        {
            err.printStackTrace();
        }
    }   
    
//    public void AppendHTML ()
//    {
//        //Finds Location of Report
//        
//        ExtentLocation = reportDirectory + "\\extentReport.html" ;
//        ExtentWrite = reportDirectory+"\\extentReportTest.html";
//        //Appends Extent with Logo
//        
//        try
//        {
//        //Reads in the html file    
//        File input = new File(ExtentLocation);
//        Document doc = Jsoup.parse(input,"UTF-8", "");
//        //Edits the Broken Time display, to show an Image instead
//        Element Panel = doc.getElementsByAttributeValue("class", "card suite-total-time-overall").first();
//        Panel.text("");
//        
//        String workingDir = System.getProperty("user.dir");
//        //Convert the Logo to Base64
//        String Image64 = convertPNGToBase64(workingDir + "\\DVT Logo.png");
//        
//        Panel.appendElement("img").attr("src","data:image/png;base64,"+Image64);
//                //Writes the Changes over the old file
//        BufferedWriter htmlWriter = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(ExtentWrite), "UTF-8"));
////        System.out.println("\n" + doc.outerHtml());
//        htmlWriter.write(doc.toString());
//        htmlWriter.flush();
//        htmlWriter.close();
//        
//        
//        }
//        catch(Exception e)
//        {
//            logError("Failed to Create an appened ExtentReport, error message -  " +e.getMessage());
//        }
//    }
    
    //Returns data from the spreadsheet
//    public String getData(String data) {
//        return testData.getData(data);
//    }

    //Checks the length of the message
    private String checkMessageLen(String message) 
    {
        String newMessage = message;

        try
        {
            //checks if message is longer than 60 characters, if so then remove a word. LOOP
            while (newMessage.length() > 60) 
            {
                newMessage = newMessage.split(" ", 2)[1];
            }
        }
        catch(Exception ex)
        {
            logError("Failed to reduce message length - " + ex.getMessage());
        }
        return newMessage;
    }

    //Called to Calculate the total run time of all the tests and to write the final data from the Error and Info log.
//    public static void finishFile()
//    {                
//        //writeErrorInfo();
//        
//        Date endDate = new Date();
//        Date testDate = new Date();
//        testDate.setSeconds(endDate.getSeconds()-startDate.getSeconds());
//        testDate.setMinutes(endDate.getMinutes()-startDate.getMinutes());
//        testDate.setHours(endDate.getHours()-startDate.getHours());
//        
//        long secs = testDate.getSeconds();
//        long mins = testDate.getMinutes();
//        long hour = testDate.getHours();
//        
//        try
//        {
//            PrintWriter writer = new PrintWriter(new FileWriter(new File(reportDirectory + "\\Narrator_Log.txt"),true));
//            writer.println(String.format("%n%-35s %-60s%n%n", "TOTAL RUN TIME: ", hour+"H:"+mins+"M:"+secs+"S"));
//            writer.close();
//        }
//        catch(IOException e)
//        {
//            e.getMessage();
//        }
//    }
    public static void logError(String error) 
    {      
        writeToLogFile("- [EROR] " + error); 
//        System.err.println("[ERR]" + error);
    }

    public static void logDebug(String debug) 
    {
        writeToLogFile("- [DBUG] " + debug);
//        System.out.println("[DEBG]" + debug);
    }
    
    public static void logPass(String failure) 
    {
        writeToLogFile("- [PASS] " + failure);
//        System.out.println("[PASS]" + failure);
    }
    
    public static void logFailure(String failure) 
    {
        writeToLogFile("- [FAIL] " + failure);
//        System.err.println("[FAIL]" + failure);
    }
    
    public static void logInfo(String info) 
    {
        writeToLogFile("- [INFO] " + info);
//        System.out.println("[INFO]" + info);
    }
    
    public static void writeToLogFile(String logMessage)
    {
        String directory = reportDirectory + "\\Narrator_Log.txt";
        File file = new File(directory);

        if (!file.exists()) 
        {
            createNewTextFile();
        }

        //Writes info to the text file        
        try 
        {
            PrintWriter writer = new PrintWriter(new FileWriter(file, true));
            writer.println(String.format(formatStr, dateFormat.format(new Date()), logMessage, "", ""));
            writer.close();
        } 
        catch (IOException e) 
        {
            System.out.printf(e.getMessage());
        }
    }
    
    public String convertPNGToBase64(String imageFilePath) 
    {
        String base64ReturnString = "";

        try 
        {
            Narrator.logInfo("[INFO] Converting screenshot to Base64 format...");
            File image = new File(imageFilePath);

            FileInputStream imageInputStream = new FileInputStream(image);

            byte imageByteArray[] = new byte[(int) image.length()];

            imageInputStream.read(imageByteArray);

            base64ReturnString = Base64.encodeBase64String(imageByteArray);

            Narrator.logInfo("[INFO] Converting completed, image ready for embedding.");
        }
        catch (Exception ex) 
        {
            Narrator.logError("[ERROR] Failed to convert image to Base64 format - " + ex.getMessage());
        }

        return base64ReturnString;
    }
    
    public static void writeToTextFile(String message, String type)
    {
        try
        {
            PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(reportDirectory + "//Add_User_Log.txt", true)));
            if (type == "") 
            {
                out.println(message);
            }
            else
            {
                out.println("[" + type + "] - " + message);
            }
            out.close();
            
        }
        catch(Exception e)
        {
            System.out.println("[ERROR] - " + e.getMessage());
        }
        
    }
}
