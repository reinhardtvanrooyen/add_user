/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.PageObjects;

import static KeywordDrivenTestFramework.Core.BaseClass.currentEnvironment;

/**
 *
 * @author Reinhardt van Rooyen
 */
public class AddUserListPageObjects 
{
    public static String AddUserURL()
    {
        return currentEnvironment.PageUrl;
    }
    public static String gatherUserData(String text)
    {
        return "//tr[@class='smart-table-data-row ng-scope']//td[3][contains(text(),'"+text+"')]";
    }
    
    public static String ButtonWithTextXpath(String text)
    {
        return "//button[text()='" + text + "']";
    }
    
    public static String InputWithName(String text)
    {
        return "//input[@name='" + text + "']";
    }
    
    public static String InputWithValueXpath(String text)
    {
        return "//input[@value='" + text + "']";
    }
    
    public static String SelectWithNameXpath(String text)
    {
        return "//select[@name='" + text + "']";
    }
   
    public static String SpanWithTextXpath(String text)
    {
        return "//span[text()='" + text + "']";
    }

    public static String TdWithTextXpath(String text)
    {
        return "//td[text()='" + text + "']";
    }
}
