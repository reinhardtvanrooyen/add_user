/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import KeywordDrivenTestFramework.Entities.TestEntity;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Reporting.Narrator;
import KeywordDrivenTestFramework.Testing.PageObjects.AddUserListPageObjects;
import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

/**
 *
 * @author Reinhardt van Rooyen
 */
public class AddUserList extends BaseClass
{

    String error = "";
    Narrator narrator;
    public String DriverExceptionDetail = "";
    public String UserURL = currentEnvironment.PageUrl;
    public int numberOfUsers = 1;
    
    public AddUserList(TestEntity testData)
    {
        this.testData = testData;
        narrator = new Narrator(testData);
    }
    
    public TestResult executeTest()
    {
        if(!navigateTo(UserURL))
        {
            return narrator.testFailed("Failed to validate the User List URL - " + error, false);
        }
        
        if(!ValidateURL())
        {
            return narrator.testFailed("Failed to validate the User List URL - " + error, false);
        }
        
        if(!AddUser1ToTheList())
        {
            return narrator.testFailed("Failed to add user1 to the user list - " + error, false);
        }
        if(!ValidateAddedUser1())
        {
            return narrator.testFailed("Failed to validate the added user1 - " + error, false);
        }
        if(!AddUser2ToTheList())
        {
            return narrator.testFailed("Failed to add user2 to the user list - " + error, false);
        }
        
        if(!ValidateAddedUser2())
        {
            return narrator.testFailed("Failed to validate the added user2 - " + error, false);
        }
        return narrator.finalizeTest("Successfully added two users to the Add User List Page", false, testData);
    }
    
    public boolean navigateTo(String pageUrl)
    {
        if(!SeleniumDriverInstance.navigateTo(AddUserListPageObjects.AddUserURL()))
        {
            error = "Failed to navigate to the Add User List URL '"+AddUserListPageObjects.AddUserURL()+"'";
            return false;
        }
        
        narrator.stepPassed("Successfully navigated to Add User List page", false);
        return true;
    }
    
    public boolean ValidateURL()
    {
        if(!SeleniumDriverInstance.waitForElementByXpathVisibility(AddUserListPageObjects.SpanWithTextXpath("User Name")))
        {
            error = "Failed to validate that the label User Name is present";
            return false;
        }
        
        narrator.stepPassed("Successfully validated the correct Add User List page", false);
        return true;
    }
    
    public boolean AddUser1ToTheList()
    {
        List <WebElement> totalUsers;
        
        if(SeleniumDriverInstance.waitForElementByXpathVisibility(AddUserListPageObjects.gatherUserData("User"), 5))
        {
            try 
            {
                totalUsers = SeleniumDriverInstance.Driver.findElements(By.xpath(AddUserListPageObjects.gatherUserData("User")));
                
            } 
            catch (Exception e) 
            {
                error = "Failed to extact a list with the username containing User" + e;
                return false;
            }
            
            for (int i = 0; i < totalUsers.size(); i++) 
            {
                numberOfUsers ++;
            }                  
        }
        
        if(!SeleniumDriverInstance.stableClickElementByXpath(AddUserListPageObjects.ButtonWithTextXpath(" Add User")))
        {
            error = "Failed to click on the Add User button";
            return false;
        }
        
        if(!SeleniumDriverInstance.stableEnterTextByXpath(AddUserListPageObjects.InputWithName("FirstName"), testData.getData("FirstName1")))
        {
            error = "Failed to enter " + testData.getData("FirstName") + " into the First Name field";
            return false;
        }
        
        if(!SeleniumDriverInstance.stableEnterTextByXpath(AddUserListPageObjects.InputWithName("LastName"), testData.getData("LastName1")))
        {
            error = "Failed to enter " + testData.getData("LastName") + " into the Last Name field";
            return false;
        }
        
        if(numberOfUsers == 0)
        {
            if(!SeleniumDriverInstance.stableEnterTextByXpath(AddUserListPageObjects.InputWithName("UserName"), testData.getData("Username")+"1"))
            {
                error = "Failed to enter " + testData.getData("Username") + " into the Username field";
                return false;
            }
        }
        else
        {
            if(!SeleniumDriverInstance.stableEnterTextByXpath(AddUserListPageObjects.InputWithName("UserName"), testData.getData("Username")+numberOfUsers))
            {
                error = "Failed to enter " + testData.getData("Username") + " into the Username field";
                return false;
            }
        }
        
        
        if(!SeleniumDriverInstance.stableEnterTextByXpath(AddUserListPageObjects.InputWithName("Password"), testData.getData("Password1")))
        {
            error = "Failed to enter " + testData.getData("Password") + " into the Password field";
            return false;
        }
        
        if(!SeleniumDriverInstance.stableClickElementByXpath(AddUserListPageObjects.InputWithValueXpath("15")))
        {
            error = "Failed to click on the Company AAA radio button";
            return false;
        }
        
        if(!SeleniumDriverInstance.stableSelectByTextFromDropDownListUsingXpath(AddUserListPageObjects.SelectWithNameXpath("RoleId"), "Admin"))
        {
            error = "Failed to select Customer from the role drop down list";
            return false;
        }
        
        if(!SeleniumDriverInstance.stableEnterTextByXpath(AddUserListPageObjects.InputWithName("Email"), testData.getData("Email1")))
        {
            error = "Failed to enter " + testData.getData("email") + " into the Email field";
            return false;
        }
        
        if(!SeleniumDriverInstance.stableEnterTextByXpath(AddUserListPageObjects.InputWithName("Mobilephone"), "0"+testData.getData("Mobilephone1")))
        {
            error = "Failed to enter " + testData.getData("Mobilephone") + " into the Mobile Phone field";
            return false;
        }
        
        if(!SeleniumDriverInstance.stableClickElementByXpath(AddUserListPageObjects.ButtonWithTextXpath("Save")))
        {
            error = "Failed to click on the Save button";
            return false;
        }
        
        narrator.stepPassed("Successfully added User1 to the user list", false);
        return true;
    }
    
    public boolean AddUser2ToTheList()
    {
        List <WebElement> totalUsers;
        
        if(SeleniumDriverInstance.waitForElementByXpathVisibility(AddUserListPageObjects.gatherUserData("User"), 5))
        {
            try 
            {
                totalUsers = SeleniumDriverInstance.Driver.findElements(By.xpath(AddUserListPageObjects.gatherUserData("User")));
                
            } 
            catch (Exception e) 
            {
                error = "Failed to extact a list with the username containing user " + e;
                return false;
            }
            
            for (int i = 0; i < totalUsers.size(); i++) 
            {
                numberOfUsers ++;
            }                  
        }
        
        if(!SeleniumDriverInstance.stableClickElementByXpath(AddUserListPageObjects.ButtonWithTextXpath(" Add User")))
        {
            error = "Failed to click on the Add User button";
            return false;
        }
        
        if(!SeleniumDriverInstance.stableEnterTextByXpath(AddUserListPageObjects.InputWithName("FirstName"), testData.getData("FirstName2")))
        {
            error = "Failed to enter " + testData.getData("FirstName") + " into the First Name field";
            return false;
        }
        
        if(!SeleniumDriverInstance.stableEnterTextByXpath(AddUserListPageObjects.InputWithName("LastName"), testData.getData("LastName2")))
        {
            error = "Failed to enter " + testData.getData("LastName") + " into the Last Name field";
            return false;
        }
        
        if(numberOfUsers == 0)
        {
            if(!SeleniumDriverInstance.stableEnterTextByXpath(AddUserListPageObjects.InputWithName("UserName"), testData.getData("Username")+"2"))
            {
                error = "Failed to enter " + testData.getData("Username") + " into the Username field";
                return false;
            }
        }
        else
        {
            if(!SeleniumDriverInstance.stableEnterTextByXpath(AddUserListPageObjects.InputWithName("UserName"), testData.getData("Username")+numberOfUsers))
            {
                error = "Failed to enter " + testData.getData("Username") + " into the Username field";
                return false;
            }
        }
        
        if(!SeleniumDriverInstance.stableEnterTextByXpath(AddUserListPageObjects.InputWithName("Password"), testData.getData("Password2")))
        {
            error = "Failed to enter " + testData.getData("Password") + " into the Password field";
            return false;
        }
        
        if(!SeleniumDriverInstance.stableClickElementByXpath(AddUserListPageObjects.InputWithValueXpath("16")))
        {
            error = "Failed to click on the Company AAA radio button";
            return false;
        }
        
        if(!SeleniumDriverInstance.stableSelectByTextFromDropDownListUsingXpath(AddUserListPageObjects.SelectWithNameXpath("RoleId"), "Customer"))
        {
            error = "Failed to select Customer from the role drop down list";
            return false;
        }
        
        if(!SeleniumDriverInstance.stableEnterTextByXpath(AddUserListPageObjects.InputWithName("Email"), testData.getData("Email2")))
        {
            error = "Failed to enter " + testData.getData("email") + " into the Email field";
            return false;
        }
        
        if(!SeleniumDriverInstance.stableEnterTextByXpath(AddUserListPageObjects.InputWithName("Mobilephone"), "0"+testData.getData("Mobilephone2")))
        {
            error = "Failed to enter " + testData.getData("Mobilephone") + " into the Mobile Phone field";
            return false;
        }
        
        if(!SeleniumDriverInstance.stableClickElementByXpath(AddUserListPageObjects.ButtonWithTextXpath("Save")))
        {
            error = "Failed to click on the Save button";
            return false;
        }
        
        narrator.stepPassed("Successfully added User2 to the user list", false);
        return true;
    }
    
    public boolean ValidateAddedUser1()
    {
        
        if(!SeleniumDriverInstance.waitForElementByXpathVisibility(AddUserListPageObjects.TdWithTextXpath(testData.getData("FirstName1")), 3))
        {
            error = "Failed to validate that " + testData.getData("FirstName1") + " has been added to the list";
            return false;
        }
        
        if(!SeleniumDriverInstance.waitForElementByXpathVisibility(AddUserListPageObjects.TdWithTextXpath(testData.getData("LastName1")), 3))
        {
            error = "Failed to validate that " + testData.getData("LastName1") + " has been added to the list";
            return false;
        }
        
        if(!SeleniumDriverInstance.waitForElementByXpathVisibility(AddUserListPageObjects.TdWithTextXpath("Admin"), 3))
        {
            error = "Failed to validate that Admin role has been added to the list for "+ testData.getData("FirstName1");
            return false;
        }
        
        if(!SeleniumDriverInstance.waitForElementByXpathVisibility(AddUserListPageObjects.TdWithTextXpath(testData.getData("Email1")), 3))
        {
            error = "Failed to validate that " + testData.getData("Email1") + " has been added to the list";
            return false;
        }
        
        if(!SeleniumDriverInstance.waitForElementByXpathVisibility(AddUserListPageObjects.TdWithTextXpath("0"+testData.getData("Mobilephone1")), 3))
        {
            error = "Failed to validate that " + testData.getData("Mobilephone1") + " has been added to the list";
            return false;
        }
        
        narrator.stepPassed("Successfully validated User1 has been added to the User List", false);
        return true;
    }
    
    public boolean ValidateAddedUser2()
    {
        if(!SeleniumDriverInstance.waitForElementByXpathVisibility(AddUserListPageObjects.TdWithTextXpath(testData.getData("FirstName2")), 3))
        {
            error = "Failed to validate that " + testData.getData("FirstName1") + " has been added to the list";
            return false;
        }
        
        if(!SeleniumDriverInstance.waitForElementByXpathVisibility(AddUserListPageObjects.TdWithTextXpath(testData.getData("LastName2")), 3))
        {
            error = "Failed to validate that " + testData.getData("LastName1") + " has been added to the list";
            return false;
        }
        
        if(!SeleniumDriverInstance.waitForElementByXpathVisibility(AddUserListPageObjects.TdWithTextXpath("Customer"), 3))
        {
            error = "Failed to validate that Admin role has been added to the list for "+ testData.getData("FirstName2");
            return false;
        }
        
        if(!SeleniumDriverInstance.waitForElementByXpathVisibility(AddUserListPageObjects.TdWithTextXpath(testData.getData("Email2")), 3))
        {
            error = "Failed to validate that " + testData.getData("Email1") + " has been added to the list";
            return false;
        }
        
        if(!SeleniumDriverInstance.waitForElementByXpathVisibility(AddUserListPageObjects.TdWithTextXpath("0"+testData.getData("Mobilephone2")), 3))
        {
            error = "Failed to validate that " + testData.getData("Mobilephone1") + " has been added to the list";
            return false;
        }
        
        narrator.stepPassed("Successfully validated User2 has been added to the User List", false);
        return true;
    }
}

