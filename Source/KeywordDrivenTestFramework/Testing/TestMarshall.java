/*
 * Changed made by James Joubert
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.browserType;
import static KeywordDrivenTestFramework.Core.BaseClass.inputFilePath;
import static KeywordDrivenTestFramework.Core.BaseClass.reportDirectory;
import static KeywordDrivenTestFramework.Core.BaseClass.reportGenerator;
import static KeywordDrivenTestFramework.Core.BaseClass.testCaseId;
import static KeywordDrivenTestFramework.Core.BaseClass.testDataList;
import KeywordDrivenTestFramework.Entities.Enums;
import KeywordDrivenTestFramework.Entities.TestEntity;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Reporting.CSVBatchReporter;
import KeywordDrivenTestFramework.Reporting.Narrator;
import KeywordDrivenTestFramework.Reporting.ReportGenerator;
import KeywordDrivenTestFramework.Reporting.TestReportEmailerUtility;
import KeywordDrivenTestFramework.Testing.TestClasses.AddUserList;
import KeywordDrivenTestFramework.Utilities.ApplicationConfig;
import KeywordDrivenTestFramework.Utilities.CSVReportUtility;
import KeywordDrivenTestFramework.Utilities.CsvReportConverter;
import KeywordDrivenTestFramework.Utilities.ExcelReaderUtility;
import KeywordDrivenTestFramework.Utilities.SeleniumDriverUtility;
import KeywordDrivenTestFramework.Utilities.VPNConnect;
import com.relevantcodes.extentreports.ExtentReports;
import java.io.*;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author fnell
 * @editir jjoubert
 */
public class TestMarshall extends BaseClass
{

    // Handles calling test methods based on test parameters , instantiates Selenium Driver object
    ExtentReports extentReports;
    ExcelReaderUtility excelInputReader;
    CSVReportUtility cSVReportUtility;
    TestReportEmailerUtility reportEmailer;
    //James removed(Moved to Narrator)
//    PrintStream errorOutputStream;
//    PrintStream infoOutputStream;
    private String dateTime;
    private int totalIgnore = 0;

    int numberOfTest = 0;
    
    public static String regressionDateTime = "";

    public ArrayList dashBoardData = new ArrayList();

    public static boolean jenkins;
   ReportGenerator reportGenerator2;

    //Default
    public TestMarshall()
    {
        inputFilePath = ApplicationConfig.InputFileName();
        testDataList = new ArrayList<>();
        excelInputReader = new ExcelReaderUtility();
        browserType = ApplicationConfig.SelectedBrowser();
        reportGenerator = new ReportGenerator(inputFilePath, ApplicationConfig.ReportFileDirectory());
        SeleniumDriverInstance = new SeleniumDriverUtility(browserType);
        this.generateReportDirectory();
    }

    //Single test pack
    public TestMarshall(String inputFilePathIn)
    {
        inputFilePath = inputFilePathIn;
        testDataList = new ArrayList<>();
        excelInputReader = new ExcelReaderUtility();
        cSVReportUtility = new CSVReportUtility(inputFilePath);
        cSVReportUtility.createCSVReportDirectoryAndFile();
        browserType = ApplicationConfig.SelectedBrowser();
        reportGenerator = new ReportGenerator(inputFilePath, ApplicationConfig.ReportFileDirectory());
        SeleniumDriverInstance = new SeleniumDriverUtility(browserType);
        this.generateReportDirectory();
    }

    //Single test pack
    //Reporting - true/false
    public TestMarshall(String inputFilePathIn, boolean jenkins)
    {
        inputFilePath = inputFilePathIn;
        testDataList = new ArrayList<>();
        excelInputReader = new ExcelReaderUtility();
        cSVReportUtility = new CSVReportUtility(inputFilePath);
        cSVReportUtility.createCSVReportDirectoryAndFile();
        browserType = ApplicationConfig.SelectedBrowser();
        reportGenerator = new ReportGenerator(inputFilePath, ApplicationConfig.ReportFileDirectory());
        SeleniumDriverInstance = new SeleniumDriverUtility(browserType);
        this.generateReportDirectory();
        this.jenkins = jenkins;
    }

    //Batch Runs
    //Reporting - true/false
    public TestMarshall(ArrayList inputFilePathIn, String inputFilePathInSingle, boolean jenkins)
    {
        inputFilePath = inputFilePathInSingle;
        inputFilePathArrayList = inputFilePathIn;
        testDataList = new ArrayList<>();
        excelInputReader = new ExcelReaderUtility();
//        cSVReportUtility = new CSVReportUtility(inputFilePath);
//        cSVReportUtility.createCSVReportDirectoryAndFile();
        browserType = ApplicationConfig.SelectedBrowser();
        reportGenerator = new ReportGenerator(inputFilePathArrayList, ApplicationConfig.ReportFileDirectory());
        SeleniumDriverInstance = new SeleniumDriverUtility(browserType);
        this.generateReportDirectory();
        this.jenkins = jenkins;
    }

    //Batch Runs
    //Browser Type Override
    //Report Folder Name
    public TestMarshall(ArrayList inputFilePathIn, String inputFilePathInSingle, Enums.BrowserType browserTypeOverride, String testFolderName, boolean jenkins)
    {
        inputFilePath = inputFilePathInSingle;
        inputFilePathArrayList = inputFilePathIn;
        testDataList = new ArrayList<>();
        excelInputReader = new ExcelReaderUtility();
//        cSVReportUtility = new CSVReportUtility(inputFilePath);
//        cSVReportUtility.createCSVReportDirectoryAndFile();
        browserType = browserTypeOverride;
        ApplicationConfig.browserType = browserType;
        reportGenerator = new ReportGenerator(inputFilePathArrayList, ApplicationConfig.ReportFileDirectory());
        SeleniumDriverInstance = new SeleniumDriverUtility(browserType);
        this.generateReportDirectory(testFolderName);

        dashBoardUtil.setTestName(testFolderName);
        dashBoardUtil.setDateTime(this.generateDateTimeString().toString());
        dashBoardUtil.setBrowser(ApplicationConfig.browserType.toString());

        dashBoardUtil.setEnvironment(currentEnvironment.PageUrl);

        this.jenkins = jenkins;
    }

    //Single test pack
    //Browser Type Override
    public TestMarshall(String inputFilePathIn, Enums.BrowserType browserTypeOverride)
    {
        inputFilePath = inputFilePathIn;
        testDataList = new ArrayList<>();
        excelInputReader = new ExcelReaderUtility();
        cSVReportUtility = new CSVReportUtility(inputFilePath);
        cSVReportUtility.createCSVReportDirectoryAndFile();
        browserType = browserTypeOverride;
        ApplicationConfig.browserType = browserType;
        reportGenerator = new ReportGenerator(inputFilePath, ApplicationConfig.ReportFileDirectory());
        SeleniumDriverInstance = new SeleniumDriverUtility(browserType);
        this.generateReportDirectory();
    }

    public void runKeywordDrivenTests() throws FileNotFoundException
    {
        System.out.println("Jenkins: " + jenkins);
//
        VPNConnect con = new VPNConnect();
        con.disconnectVPN();
        if (!con.connectToVPN())
        {
            System.out.println("[ERROR] - Failed to start the VPN");
        }
//        this.redirectOutputStreams();

        //Validate all test packs exists and remove the ones that dont
        if (inputFilePathArrayList != null)
        {
            for (int i = 0; i < inputFilePathArrayList.size(); i++)
            {
                File f = new File(inputFilePathArrayList.get(i).toString());
                if (f.exists() && !f.isDirectory())
                {
                    //Do nothing
                }
                else
                {
                    System.err.println("[Error] Test Pack: " + inputFilePathArrayList.get(i) + " was not found at the location specified - test pack will be removed from the list of test packs to be executed");
                    inputFilePathArrayList.remove(i);
                }
            }
        }
        testDataList.clear();
        //Load test data
        if (inputFilePathArrayList != null)
        {
            for (int i = 0; i < inputFilePathArrayList.size(); i++)
            {
                testDataList = loadTestData((String) inputFilePathArrayList.get(i));
            }
        }
        else
        {
            testDataList = loadTestData(inputFilePath);
        }

//        testDataList = loadTestData(inputFilePath);
        System.out.println("TEST DATA LIST SIZE: " + testDataList.size());
        if (testDataList.size() < 1)
        {
            Narrator.logError("Test data object is empty - spreadsheet not found or is empty");
        }
        else
        {
            reportGenerator.creatingNewTextFile();
            //James added
//            CheckBrowserExists();
//            Narrator.takeScreenShot(true, "init");
            // Each case represents a test keyword found in the excel spreadsheet

//            for (TestEntity testData : testDataList)
//            {
//                System.out.println("Test Case ID: " + testData.TestCaseId);
//            }
            for (TestEntity testData : testDataList)
            {
                if (inputFilePathArrayList!=null) 
                {
                   if (inputFilePathArrayList.contains(testData.InputFileName)) 
                    {
                        inputFilePathArrayList.remove(testData.InputFileName);
                    } 
                }              
                Narrator.writeToTextFile(testData.TestMethod, "TEST NAME");
                testCaseId = testData.TestCaseId;
                // Make sure browser is not null - could have thrown an exception and terminated
//                CheckBrowserExists();
                // Skip test methods and test case id's starting with ';'
                if (!testData.TestMethod.startsWith(";") && !testData.TestCaseId.startsWith(";"))
                {
                    Narrator.logDebug("Executing test - " + testData.TestMethod);
                    System.out.println("Executing: " + testData.TestCaseId + " - " + testData.TestMethod);
                    Narrator.writeToTextFile(testData.TestCaseId, "TEST");
                    switch (testData.TestMethod)
                    {
                        
                        case "AddUserList":
                        {
                            ensureNewBrowserInstance();
                            AddUserList addUserList = new AddUserList(testData);
                            reportGenerator.addResult(addUserList.executeTest());
                            numberOfTest++;
                            break;
                        }
                    }
                    int index = reportGenerator.testResults.size()-1;
               
                    if (reportGenerator.testResults.get(index).testStatus.toString().equals("FAIL")) 
                    {
                       reportGenerator2.generateTestReport2(); 
                    }
                    
                    terminateTest(reportGenerator, testData);

                    reportTextFile(numberOfTest);
                    Narrator.logDebug("Continuing to next test method");
                }
                Narrator.writeToTextFile("***************************", "");
                //James added
                if (testData.TestMethod.startsWith(";") || testData.TestCaseId.startsWith(";"))
                {
                    totalIgnore++;
                    reportGenerator.writeToFile4(numberOfTest, testDataList.size(), totalIgnore);
                }
            }
            
            
            
            
            
            SeleniumDriverInstance.shutDown();
            reportGenerator.generateTestReport();
            reportEmailer = new TestReportEmailerUtility(reportGenerator.testResults);
            reportEmailer.SendResultsEmail();

            //James removed(Moved to Narrator)
            //this.flushOutputStreams();
            //James added
            //Narrator.finishFile();
        }
        con.disconnectVPN();
        //openTestReport();
        

//        if (jenkins == true)
//        {
//            DataBaseUtility dbUtil = new DataBaseUtility();
//            try
//            {
//                dbUtil.insertIntoReportingDB(dashBoardUtil.getTestName(), dashBoardUtil.getDateTime(), dashBoardUtil.getBrowser(), dashBoardUtil.getTotalPasses(), dashBoardUtil.getTotalFailures(), dashBoardUtil.getDuration());
//                ArrayList tempTestCaseId = new ArrayList();
//                ArrayList tempTest = new ArrayList();
//                ArrayList tempTestStatus = new ArrayList();
//                tempTestCaseId = dashBoardUtil.getTestCaseIdArray();
//                tempTest = dashBoardUtil.getTestArray();
//                tempTestStatus = dashBoardUtil.getStatusArray();
//                for (int i = 0; i < tempTestCaseId.size(); i++)
//                {
//                    dbUtil.insertIntoReportingDetailedDB(dashBoardUtil.getTestName(), tempTestCaseId.get(i).toString(), tempTest.get(i).toString(),
//                            dashBoardUtil.getDateTime(), dashBoardUtil.getBrowser(), tempTestStatus.get(i).toString(), dashBoardUtil.getEnvironment());
//                }
//            }
//            catch(Exception e)
//            {
//                System.err.println("[ERROR] -  Failed to insert the results into the reporting database");
//            }
//        }
//        con.DriverTaskkill();
    }

    public void openTestReport()
    {
        try
        {
            String[] reportsFolderPathSplit = this.reportDirectory.split("\\\\");
            String reportLocation = System.getProperty("user.dir") +"\\"+ ApplicationConfig.ReportFileDirectory()+reportsFolderPathSplit[reportsFolderPathSplit.length - 1] + "\\" + "extentReport.html";
            
            String openReport = "explorer \""+reportLocation+"\"";
            Runtime.getRuntime().exec(openReport);
        }
        catch(Exception ex)
        {
            System.out.println("[ERROR] -f " + ex.getMessage());
        }
    }
    
    public void terminateTest(ReportGenerator generator, TestEntity testData)
    {
        for (TestResult result : generator.testResults)
        {
            if (result.testStatus == Enums.ResultStatus.FAIL)
            {
                //Run API
                //Run the rebuild class
            }
        }
    }

    private List<TestEntity> loadTestData(String inputFilePath)
    {
        System.out.println("INPUT FILE PATH: " + inputFilePath);
        return excelInputReader.getTestDataFromExcelFile(inputFilePath);
    }

    public static void CheckBrowserExists()
    {
        if (SeleniumDriverInstance == null)
        {
            SeleniumDriverInstance = new SeleniumDriverUtility(browserType);
            SeleniumDriverInstance.startDriver();
        }

        if (!SeleniumDriverInstance.isDriverRunning())
        {
            SeleniumDriverInstance.startDriver();
        }
    }

    public static void ensureNewBrowserInstance()
    {
        if (SeleniumDriverInstance.isDriverRunning())
        {
            SeleniumDriverInstance.shutDown();
        }
        SeleniumDriverInstance.startDriver();
    }

    public String generateDateTimeString()
    {
        Date dateNow = new Date();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss");
        dateTime = dateFormat.format(dateNow).toString();
        return dateTime;
    }

    public void generateReportDirectory()
    {
        reportDirectory = ApplicationConfig.ReportFileDirectory() + resolveScenarioName()+ "_" + this.generateDateTimeString();
//         + "_" + this.generateDateTimeString
        String[] reportsFolderPathSplit = this.reportDirectory.split("\\\\");
        this.currentTestDirectory = ApplicationConfig.ReportFileDirectory() + reportsFolderPathSplit[reportsFolderPathSplit.length - 1];

//        File files = new File(reportDirectory);
//	if (!files.exists())
//        {
//            if (files.mkdirs())
//            {
//                System.out.println("Directory for reports created");
//            }
//
//	}
    }

    public void generateReportDirectory(String reportFolderName)
    {
        reportDirectory = ApplicationConfig.ReportFileDirectory() +  reportFolderName + "_" + this.generateDateTimeString();
        String[] reportsFolderPathSplit = this.reportDirectory.split("\\\\");
        this.currentTestDirectory = ApplicationConfig.ReportFileDirectory() +  reportsFolderPathSplit[reportsFolderPathSplit.length - 1];
    }

//    public void redirectOutputStreams()
//    {
//        try
//        {
//            File reportDirectoryFile = new File(reportDirectory);
//            reportDirectoryFile.mkdirs();
//
//            FileOutputStream infoFile = new FileOutputStream(reportDirectory + "\\" + "InfoTestLog.txt");
//            FileOutputStream errorFile = new FileOutputStream(reportDirectory + "\\" + "ErrorTestLog.txt");
//
//            MultiOutputStream multiOut = new MultiOutputStream(System.out,infoFile);
//            MultiOutputStream multiErr = new MultiOutputStream(System.err,errorFile);
//
//            infoOutputStream = new PrintStream(multiOut);
//            errorOutputStream = new PrintStream(multiErr);
//
//            System.setOut(infoOutputStream);
//            System.setErr(errorOutputStream);
//        }
//        catch (FileNotFoundException ex)
//        {
//            System.err.println("[Error] could not create log files - " + ex.getMessage());
//        }
//    }
//
//    public void flushOutputStreams()
//    {
//
//        errorOutputStream.flush();
//        infoOutputStream.flush();
//
//        errorOutputStream.close();
//        infoOutputStream.close();
//
//    }
    //James removed(Moved to Narrator)
//    public void redirectOutputStreams() {
//        try {
//            File reportDirectoryFile = new File(reportDirectory);
//            File errorFile = new File(reportDirectory + "\\" + "ErrorTestLog.txt");
//            File infoFile = new File(reportDirectory + "\\" + "InfoTestLog.txt");
//            File narrator = new File(reportDirectory + "\\Narrator_Log.txt");
//            reportDirectoryFile.mkdirs();
//
//            errorOutputStream = new PrintStream(errorFile);//errorFile
//            infoOutputStream = new PrintStream(infoFile);//infoFile
//
//            System.setOut(infoOutputStream);
//
//            System.setErr(errorOutputStream);
//
//        } catch (FileNotFoundException ex) {
//            Narrator.errorLog(" could not create log files - " + ex.getMessage());
//        }
//    }
    //James removed (Moved to Narrator)
//    public void flushOutputStreams() {
//
//        errorOutputStream.flush();
//        infoOutputStream.flush();
//
//        errorOutputStream.close();
//        infoOutputStream.close();
//
//    }
    public void reportTextFile(int numberOfTests)
    {
        reportGenerator.writeToFile4(numberOfTests, testDataList.size(), totalIgnore);
    }
    
    
    public void setRegressionCSVName(String input)
    {
        regressionDateTime = input;
    }
    
    public void generateBatchStats(String csvName)
    {
        CSVBatchReporter genStats = new CSVBatchReporter(System.getProperty("user.dir")+"\\CSVReports\\RegressionReports",csvName);
        genStats.generateStats();
    }
    
    public void convertAndStyleCSVToXLSX(String csvPath, String sheetName)
    {
        CsvReportConverter convert = new CsvReportConverter();
        convert.csvToXLSX(System.getProperty("user.dir")+"\\CSVReports\\RegressionReports\\"+csvPath+".csv", sheetName);
    }
    
}
