/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Utilities;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

/**
 *
 * @author trichardson
 */
public class CsvReportConverter 
{
    XSSFWorkbook workBook;
    
    public CsvReportConverter()
    {
    }
    
    public void csvToXLSX(String csvPathAbsPath, String sheetName) 
    {
        File csvFile = new File (csvPathAbsPath);
        String fileStr = csvFile.toString();
        if(csvFile.exists())
        {
            try 
            {
                String csvFileAddress = csvPathAbsPath; //csv file address
                String xlsxFileAddress = csvPathAbsPath.replace(".csv", ".xlsx"); //xlsx file address
                workBook = new XSSFWorkbook();
                XSSFSheet sheet = workBook.createSheet(sheetName);

                String currentLine=null;
                int RowNum=0;

                BufferedReader br = new BufferedReader(new FileReader(csvFileAddress));

                while ((currentLine = br.readLine()) != null) 
                {
                    String str[] = currentLine.split(",");
                    RowNum++;
                    XSSFRow currentRow=sheet.createRow(RowNum);

                    for(int i=0;i<str.length;i++)
                    {
//                        XSSFCellStyle style = workBook.createCellStyle();
//                        
//                        switch(i)
//                        {
//                            //TEST CASE ID
//                            case 0:
//                            {
//                                style = getStyle(str[i]);
//                            }
//                            //STATUS
//                            case 1:
//                            {
//                                
//                            }
//                            //SCRIPT RETURNED MESSAGE
//                            case 2:
//                            {
//                                
//                            }
//                            //TIME TAKEN
//                            case 3:
//                            {
//                                
//                            }
//                            //ENVIRONMENT
//                            case 4:
//                            {
//                                
//                            }
//                            
//                        }
//                        
//
//                        
//                        style.setFillForegroundColor(new XSSFColor(new java.awt.Color(128,0, 128)));
//                        
                        
                        currentRow.createCell(i).setCellValue(str[i]);
                        //currentRow.getCell(i).getCellStyle().setFillBackgroundColor(style);
                    }
                }

                FileOutputStream fileOutputStream =  new FileOutputStream(xlsxFileAddress);
                workBook.write(fileOutputStream);
                fileOutputStream.close();
                System.out.println("Updated xlsx output file - '"+xlsxFileAddress+"'");
            } 
            catch (Exception ex) 
            {
                System.out.println("[ERROR] - "+ex.getMessage());
            }
        }
        else
        {
            System.out.println("[ERROR] - Could not find the specified file '"+csvPathAbsPath+"'");
        }
        
    }   
   
    
//    private XSSFCellStyle getStyle(String input)
//    {
//        XSSFCellStyle returnStyle = workBook.createCellStyle();
//        
//        switch(input)
//        {
//            
//        }
//        
//        return returnStyle;
//    }
    
    
    
}
