/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Utilities;

import java.util.ArrayList;

/**
 *
 * @author szeuch
 */
public class DashboardReportingUtility 
{
    //---Test Pack Breakdown---//
    String testName;
    String dateTime;
    String browser;
    int totalTests;
    int totalPasses;
    int totalFailures;
    String duration;
    //---Test Pack Breakdown---//
    
    //---Test Case Breakdown---//
    ArrayList testCaseIdArray = new ArrayList<String>();
    ArrayList testArray = new ArrayList<String>();
//    ArrayList dateTimeArray = new ArrayList<String>();
    ArrayList statusArray = new ArrayList<String>();
    String environment;
    //---Test Case Breakdown---//
    
    public void reset()
    {
        this.setTestName("");
        this.setDateTime("");
        this.setBrowser("");
        this.setTotalTests(0);
        this.setTotalPasses(0);
        this.setTotalFailures(0);
        this.setDuration("");
        this.testCaseIdArray.clear();
        this.testArray.clear();
        this.statusArray.clear();
        this.setEnvironment("");
    }

    //---Test Pack Breakdown---//
    public String getTestName() 
    {
        return testName;
    }

    public void setTestName(String testName) 
    {
        this.testName = testName;
    }

    public String getDateTime()
    {
        return dateTime;
    }

    public void setDateTime(String dateTime) 
    {
        this.dateTime = dateTime;
    }

    public String getBrowser()
    {
        return browser;
    }

    public void setBrowser(String browser)
    {
        this.browser = browser;
    }

    public int getTotalTests()
    {
        return totalTests;
    }

    public void setTotalTests(int totalTests)
    {
        this.totalTests = totalTests;
    }

    public int getTotalPasses() 
    {
        return totalPasses;
    }

    public void setTotalPasses(int totalPasses)
    {
        this.totalPasses = totalPasses;
    }

    public int getTotalFailures()
    {
        return totalFailures;
    }

    public void setTotalFailures(int totalFailures)
    {
        this.totalFailures = totalFailures;
    }

    public String getDuration()
    {
        return duration;
    }

    public void setDuration(String duration) 
    {
        this.duration = duration;
    }
    //---Test Pack Breakdown---//

    //---Test Case Breakdown---//
    public ArrayList getTestCaseIdArray() 
    {
        return testCaseIdArray;
    }
    
    public void addTestCaseIdArray(String testCaseId) 
    {
        this.testCaseIdArray.add(testCaseId);
    }

    public ArrayList getTestArray()
    {
        return testArray;
    }

    public void addTestArray(String test)
    {
        this.testArray.add(test);
    }

//    public ArrayList getDateTimeArray()
//    {
//        return dateTimeArray;
//    }
//
//    public void addDateTimeArray(String dateTimeArray) 
//    {
//        this.dateTimeArray.add(dateTimeArray);
//    }

    public ArrayList getStatusArray() 
    {
        return statusArray;
    }

    public void addStatusArray(String status) 
    {
        this.statusArray.add(status);
    }

    public String getEnvironment()
    {
        return environment;
    }

    public void setEnvironment(String environment) 
    {
        this.environment = environment;
    }
    //---Test Case Breakdown---//
}
