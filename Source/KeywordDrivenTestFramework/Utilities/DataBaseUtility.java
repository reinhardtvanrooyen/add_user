/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package KeywordDrivenTestFramework.Utilities;

import KeywordDrivenTestFramework.Core.BaseClass;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
/**
 *
 * @author om22925
 */
public class DataBaseUtility extends BaseClass
{

    private Connection conn;
    private Statement stmt;
    private ResultSet rs;
    ApplicationConfig appConfig = new ApplicationConfig();
    
    public void initVar() 
    {
        conn = null;
        stmt = null;
        rs = null;
    }

    public static void closeRS(ResultSet rs) 
    {
        try 
        {
            if (rs != null) 
            {
                rs.close();
            }
        }
        catch (Exception ex) 
        {
            System.err.println("Error unable to close RS, fault -" + ex.getMessage());
        }
    }

    public static void closeStmt(Statement stmt)
    {
        try 
        {
            if (stmt != null) 
            {
                stmt.close();
            }
        } 
        catch (Exception ex)
        {
            System.err.println("Error unable to close Statement, fault -" + ex.getMessage());
        }
    }

    public static void closeConnection(Connection conn) 
    {
        try
        {
            if (conn != null) 
            {
                conn.close();
            }
        }
        catch (Exception ex) 
        {
            System.err.println("Error unable to close Connection, fault -" + ex.getMessage());
        }
    }
    
    public void closeDB() 
    {
        closeRS(rs);
        closeStmt(stmt);
        closeConnection(conn);      
    }
    
    public Connection formConnection(String connectionString, String dbUser, String dbPassword)
    {
        try
        {
            Class.forName(("com.mysql.jdbc.Driver")).newInstance();
            Connection con = DriverManager.getConnection(connectionString, dbUser, dbPassword);
            System.out.println("[Info] Connection to db: " + connectionString + " found");
            return con;
        }
        catch(Exception e)
        {
            System.err.println("[Error] Connection to db: " + connectionString + " not found - " + e.getMessage());            
            return null;
        }
        
    }
    
    public Boolean connectToDB()  
    {
        try
        {
            conn = formConnection("jdbc:mysql://dvt-sqa12/automation_reporting", "root", "password");

            System.out.println("[Info] Successfully connected to the " + "automation_reporting"+ " db");
            return true;
        }
    
        catch(Exception e)
        {
//            System.err.println("[Error] Unable to connect to the " + ApplicationConfig.dbNameLocal()+ " db, fault - "+e.getMessage());
            return false;
        }
            
    }
    
    public boolean insertIntoReportingDB(String testName, String dateTime, String browser, int totalPasses, int totalFailures, String duration) 
    {
        initVar();
        try 
        {
            int totalTests = totalPasses + totalFailures;
            
            connectToDB();
    
            stmt = conn.createStatement();
            
            String sql = "INSERT INTO regression_results_sintrex (test_name, date_time, browser, total_tests, total_passes, total_failures, duration)\n" +
                        "VALUES ('" + testName + "', '" + dateTime + "','" + browser + "'," + totalTests + "," + totalPasses + "," + totalFailures + ",'" + duration + "');";
            
            stmt.executeUpdate(sql);
            
            System.out.println("[Info] Successfully inserted a report result into the database");
            
            closeDB();
            return true;
            
        } 
        catch (Exception ex) 
        {
            System.err.println("Error unable to insert, fault -" + ex.getMessage());
            closeDB();
            return false;
        }
    }
    
    public boolean insertIntoReportingDetailedDB(String testName, String td_id, String test, String dateTime, String browser, String status, String env) 
    {
        initVar();
        try 
        {
            connectToDB();
            
            stmt = conn.createStatement();
            
            String sql = "INSERT INTO regression_results_detailed_sintrex (test_name, tc_id, test, date_time, browser, status, environment)\n" +
                        "VALUES ('" + testName + "', '" + td_id + "','" + test + "','" + dateTime + "','" + browser + "','" + status + "','" + env + "');";
            
            stmt.executeUpdate(sql);
            
            System.out.println("[Info] Successfully inserted a detailed report result into the database");
            
            closeDB();
            return true;
            
        }
        catch (Exception ex) 
        {
            System.err.println("Error unable to insert, fault -" + ex.getMessage());
            closeDB();
            return false;
        }
    }
    
    public ArrayList retrieveFromDB(int amount, String tableName, int productCode, String date, String componentDescription, String relationship, String dateOfBirth,String sumAssured)  
    {
        ArrayList contractNumbers = new ArrayList();
        initVar();
        try 
        {
//            connectToDBMIP();
            String sql ="SELECT TOP " + amount + " [RowId]\n" +
                "      ,[Business_Type]\n" +
                "      ,[Contract_Number]\n" +
                "      ,[Contract_Reference]\n" +
                "      ,[Contract_Status]\n" +
                "      ,[Product_Code]\n" +
                "      ,[Product_Taxable]\n" +
                "      ,[Commencement_Date]\n" +
                "      ,[Inception_Date]\n" +
                "      ,[Campaign_Code]\n" +
                "      ,[Call_Centre_Code]\n" +
                "      ,[Call_Centre_Fee]\n" +
                "      ,[Brokerage_Code]\n" +
                "      ,[Broker_Fee]\n" +
                "      ,[MoneyBack_Percentage]\n" +
                "      ,[GL_Scheme_Code]\n" +
                "      ,[Restart_Date]\n" +
                "      ,[Component_Code]\n" +
                "      ,[Component_Obj]\n" +
                "      ,[Component_Description]\n" +
                "      ,[Component_Number]\n" +
                "      ,[Component_Status]\n" +
                "      ,[Component_Start_Date]\n" +
                "      ,[Component_End_Date]\n" +
                "      ,[First_Component_Effective_Date]\n" +
                "      ,[Component_Premium_Recurring]\n" +
                "      ,[Component_Premium_Single]\n" +
                "      ,[Component_Sum_Assured_Recurring]\n" +
                "      ,[Component_Sum_Assured_Single]\n" +
                "      ,[Main_Benefit]\n" +
                "      ,[Role_Player_Obj]\n" +
                "      ,[Role_Player_Effective_From_Date]\n" +
                "      ,[Role_Player_Relationship]\n" +
                "      ,[Role_Player_Code]\n" +
                "      ,[Person_Obj]\n" +
                "      ,[Gender]\n" +
                "      ,[DateOfBirth]\n" +
                "      ,[SmokerStatus]\n" +
                "      ,[Education]\n" +
                "      ,[Salary]\n" +
                "      ,[Reinsurance_Prop]\n" +
                "      ,[Reinsurance_Premium]\n" +
                "      ,[Reinsurance_Cover]\n" +
                "      ,[Reinsurance_Component_Code]\n" +
                "      ,[Reinsurance_Component_Description]\n" +
                "      ,[Expiry_Date]\n" +
                "      ,[Collection_Frequency]\n" +
                "      ,[Premium_Balance]\n" +
                "      ,[Premium]\n" +
                "      ,[Commission_Percentage]\n" +
                "      ,[Commission_Type]\n" +
                "      ,[Escalation_Effective_Date]\n" +
                "      ,[Escalation_Amount]\n" +
                "      ,[Escalation_End_Date]\n" +
                "      ,[Escalation_Frequency]\n" +
                "      ,[Escalation_Month]\n" +
                "      ,[Escalation_Premium_Increase]\n" +
                "      ,[Escalation_Cover_Increase]\n" +
                "      ,[Escalation_Type]\n" +
                "      ,[Escalation_Cease_Age]\n" +
                "      ,[On_Movement]\n" +
                "      ,[On_Movement_Audit_Date]\n" +
                "      ,[Off_Movement]\n" +
                "      ,[Off_Movement_Audit_Date]\n" +
                "      ,[Contract_Component_Status]\n" +
                "      ,[Product_Component_Variable_Rate_Table]\n" +
                "      ,[Product_component_Fixed_Rate_Table]\n" +
                "      ,[Product_Component_Rate_Table]\n" +
                "      ,[Additional_Rating_Criteria]\n" +
                "      ,[Rate_Table_Name_NewBusiness]\n" +
                "      ,[Rate_Table_Name_Maintenance]\n" +
                "      ,[Rate_Table_Name_NewBusiness_Components]\n" +
                "      ,[Rate_Table_Name_Maintenance_Components]\n" +
                "      ,[ManagementFeeChargePerc]\n" +
                "      ,[ManagementFeeCharge]\n" +
                "      ,[SurrenderValue]\n" +
                "      ,[SurrenderFee]\n" +
                "      ,[AllocationChargePerc]\n" +
                "      ,[AllocationCharge]\n" +
                "      ,[InvestmentValue]\n" +
                "      ,[InvestPortf1]\n" +
                "      ,[InvestUnits1]\n" +
                "      ,[InvestPortf2]\n" +
                "      ,[InvestUnits2]\n" +
                "      ,[InvestPortf3]\n" +
                "      ,[InvestUnits3]\n" +
                "      ,[InvestPortf4]\n" +
                "      ,[InvestUnits4]\n" +
                "      ,[MortalityLoading]\n" +
                "      ,[MortalityLoadingPerc]\n" +
                "      ,[RiskPremiumLoadingCharge]\n" +
                "      ,[LoanBalance]\n" +
                "      ,[ExpenseAccountBalance]\n" +
                "      ,[Member_Person_Obj]\n" +
                "      ,[Scheme_Code]\n" +
                "      ,[Scheme_Name]\n" +
                "      ,[Scheme_Contract]\n" +
                "      ,[Conversion_Date]\n" +
//                "  FROM [MIPValex].[dbo].[" + ApplicationConfig.dbTableName() + "]\n" +
                "  WHERE Contract_Status = 'INF'\n" +
                "  AND Component_Status = 'ACTIVE'\n" +
                "  AND Role_Player_Code = 'LifeA'" + 
                "  AND Product_Code = " + productCode + "\n" +
                "  AND Commencement_Date LIKE '%" + date + "%'\n" +
                /*"  AND Commencement_Date > '" + dateStart + "'\n" + Does not retrieve the contracts between those dates
                "  AND Commencement_Date < '" + dateEnd + "'\n" +*/
                "  AND Component_Description LIKE '%" + componentDescription + "%'\n" +
                "  AND Component_Sum_Assured_Single LIKE '%" + sumAssured + "%'\n" +
                "  AND Role_Player_Relationship LIKE '%" + relationship + "%'\n" + 
                "  AND DATEPART(yy, DateOfBirth) LIKE '%" + dateOfBirth + "%'";
                
            stmt = conn.createStatement();
            rs = stmt.executeQuery(sql);
            while (rs.next())
            {
                String contractNumber = rs.getString("Contract_Number");
                if (!contractNumbers.contains(contractNumber)) 
                {
                    contractNumbers.add(rs.getString("Contract_Number"));
                }
            }
            System.out.println("[Info] Contract Numbers from MIP: " + contractNumbers.size());
            closeDB();
            return contractNumbers;
            
        } 
        catch (Exception ex) 
        {
            System.err.println("Error unable to retrieve contract numbers from MIP, fault -" + ex.getMessage());
            closeDB();
            return null;
        }
    }
}