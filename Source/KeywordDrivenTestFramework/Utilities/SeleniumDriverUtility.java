    /*
    * To change this template, choose Tools | Templates
    * and open the template in the editor.
    */
    package KeywordDrivenTestFramework.Utilities;

    import KeywordDrivenTestFramework.Core.BaseClass;
    import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.setScreenshotPath;
import static KeywordDrivenTestFramework.Core.BaseClass.testCaseId;
    import KeywordDrivenTestFramework.Entities.Enums;
    import KeywordDrivenTestFramework.Entities.RetrievedTestValues;
    import KeywordDrivenTestFramework.Entities.TestEntity;
    import KeywordDrivenTestFramework.Reporting.Narrator;
    import java.io.File;
    import java.util.List;
    import java.util.concurrent.TimeUnit;
import org.apache.commons.io.FileUtils;
    import org.monte.screenrecorder.ScreenRecorder;
    import org.openqa.selenium.*;
    import org.openqa.selenium.chrome.ChromeDriver;
    import org.openqa.selenium.chrome.ChromeOptions;
    import org.openqa.selenium.firefox.FirefoxDriver;
    import org.openqa.selenium.firefox.FirefoxProfile;
    import org.openqa.selenium.ie.InternetExplorerDriver;
    import org.openqa.selenium.interactions.Actions;
    import org.openqa.selenium.remote.DesiredCapabilities;
    import org.openqa.selenium.support.ui.ExpectedConditions;
    import org.openqa.selenium.support.ui.Select;
    import org.openqa.selenium.support.ui.WebDriverWait;


    /**
    *
    * @author fnell
    * @editir jjoubert
    */
    // Contains logic for handling accessor methods and driver calls.
    public class SeleniumDriverUtility extends BaseClass
    {

    /**
     *
     */
    public WebDriver Driver;
    private Enums.BrowserType browserType;
    File fileIEDriver;
    File fileChromeDriver;
    File fileFireFoxDriver;
    private Boolean _isDriverRunning;
    public RetrievedTestValues retrievedTestValues;
    public String DriverExceptionDetail = "";
    TestEntity testData;
    private Object document;
    private String mainWindowsHandle;
    private int count = 0;
    private ScreenRecorder screenRecorder;

    public SeleniumDriverUtility(Enums.BrowserType selectedBrowser)
    {
        retrievedTestValues = new RetrievedTestValues();

        _isDriverRunning = false;
        browserType = selectedBrowser;

        fileIEDriver = new File("IEDriverServer.exe");
        System.setProperty("webdriver.ie.driver", fileIEDriver.getAbsolutePath());

        fileChromeDriver = new File("chromedriver.exe");
        System.setProperty("webdriver.chrome.driver", fileChromeDriver.getAbsolutePath());

    //        fileFireFoxDriver = new File("geckodriver.exe");
    //        System.setProperty("webdriver.gecko.driver", fileFireFoxDriver.getAbsolutePath());


    }

    public boolean isDriverRunning()
    {
        return _isDriverRunning;
    }

    public void startDriver()
    {
        try
        {
            System.setProperty("org.apache.commons.logging.Log", "org.apache.commons.logging.impl.Jdk14Logger");

            switch (browserType)
            {
                case IE:
                    DesiredCapabilities caps = DesiredCapabilities.internetExplorer(); caps.setCapability("ignoreZoomSetting", true);
                    caps.setCapability("nativeEvents",false);
                    caps.setCapability("requireWindowFocus", true);
                  String[] dialog =  new String[]{ System.getProperty("user.dir") +"\\IEDriverServer.exe","Save to...","Save", System.getProperty("user.home") + "\\Downloads\\" }; // path to exe, dialog title, save/cancel/run, path to save file.
                  Process pp1 = Runtime.getRuntime().exec(dialog);
    //                  pp1.destroy();
                    Driver = new InternetExplorerDriver(caps);
                    Driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
                    _isDriverRunning = true;
                    break;
                case FireFox:
                    Driver = new FirefoxDriver(FirefoxDriverProfile());
                    _isDriverRunning = true;
                    break;
                case Chrome:
                    ChromeOptions options = new ChromeOptions();
                    options.addArguments("disable-infobars");
                    //options.addArguments("user-data-dir=C:\\Users\\cstemmet\\AppData\\Local\\Google\\Chrome\\User Data");
                    Driver = new ChromeDriver(options);
                    _isDriverRunning = true;
                    break;
                case Safari: ;
                    break;
            }
            retrievedTestValues = new RetrievedTestValues();
            Driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
            Driver.manage().timeouts().pageLoadTimeout(120, TimeUnit.SECONDS);
            Driver.manage().timeouts().setScriptTimeout(1, TimeUnit.SECONDS);
            Driver.manage().window().maximize();
        }
        catch (Exception e)
        {
            Narrator.logError("Error starting the driver - " + e.getMessage());
        }
    }

    public static FirefoxProfile FirefoxDriverProfile() throws Exception
    {
        try
        {
            File firePath = new File("C:\\firepath.xpi");
            File firebug = new File("C:\\firebug.xpi");

            FirefoxProfile profile = new FirefoxProfile();
            String downloadPath = System.getProperty("user.home") + "/Downloads/";
            profile.setPreference("browser.download.folderList", 2);
            profile.setPreference("browser.download.manager.showWhenStarting", false);
            profile.setPreference("browser.download.dir", downloadPath);
            profile.setPreference("browser.helperApps.neverAsk.openFile",
                    "text/csv,application/x-msexcel,application/excel,application/x-excel,application/vnd.ms-excel,image/png,image/jpeg,text/html,text/plain,application/msword,application/xml,application/zip,application/octet-stream");
            profile.setPreference("browser.helperApps.neverAsk.saveToDisk",
                    "text/csv,application/x-msexcel,application/excel,application/x-excel,application/vnd.ms-excel,image/png,image/jpeg,text/html,text/plain,application/msword,application/xml,application/zip,application/octet-stream");
            profile.setPreference("browser.helperApps.alwaysAsk.force", false);
            profile.setPreference("browser.download.manager.alertOnEXEOpen", false);
            profile.setPreference("browser.download.manager.focusWhenStarting", true);
            profile.setPreference("browser.download.manager.useWindow", false);
            profile.setPreference("browser.download.manager.showAlertOnComplete", false);
            profile.setPreference("browser.download.manager.closeWhenDone", false);
            profile.setPreference("browser.download.manager.showAllDownloads", false);

            if (firePath.isFile())
            {
                profile.addExtension(firePath);
            }

            if (firebug.isFile())
            {
                profile.addExtension(firebug);
            }

            return profile;
        }
        catch (Exception e)
        {
            Narrator.logError("Error  setting the Firefox profiles - " + e.getMessage());
            return null;
        }
    }

   public boolean navigateTo(String pageUrl)
    {
        try
        {
            Driver.navigate().to(pageUrl);
            return true;
        }
        catch (Exception e)
        {
            Narrator.logError(" navigating to URL - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }
    }
	public boolean clickElementByXpathActions(String elementXpath)
    {
        try
        {
           if (ApplicationConfig.SelectedBrowser() == Enums.BrowserType.FireFox)
           {

                try
                {
                Actions action = new Actions(SeleniumDriverInstance.Driver);
                action.release().build().perform();
                }
                catch(Exception ex)
                {

                }
            }
            Narrator.logDebug(" Clicking element by Xpath : " + elementXpath);
            waitForElementByXpath(elementXpath);
            waitForElementToBeClickableByXpath(elementXpath);
            WebDriverWait wait = new WebDriverWait(Driver, ApplicationConfig.WaitTimeout());
            wait.until(ExpectedConditions.elementToBeClickable(By.xpath(elementXpath)));
            WebElement elementToClick = Driver.findElement(By.xpath(elementXpath));
            Actions builder = new Actions(Driver);
            builder.moveToElement(elementToClick).build().perform();
            builder.click(elementToClick).build().perform();

            return true;
        }
        catch (Exception e)
        {
            Narrator.logError(" Failed to click on element by Xpath - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }
    }
	
public boolean waitForElementToBeClickableByXpath(String elementXpath)
    {
        boolean elementFound = false;
        try
        {
            int waitCount = 0;
            while (!elementFound && waitCount < ApplicationConfig.WaitTimeout())
            {
                try
                {
                    WebDriverWait wait = new WebDriverWait(Driver, 1);
                    if (wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(elementXpath))) != null)
                    {
                        if (wait.until(ExpectedConditions.elementToBeClickable(By.xpath(elementXpath))) != null)
                        {
                            elementFound = true;
                            break;
                        }
                    }
                }
                catch (Exception e)
                {
                    elementFound = false;
                }
                waitCount++;
            }

        }
        catch (Exception e)
        {
            Narrator.logError(" waiting for element to be clickable by Xpath '" + elementXpath + "' - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
        }
        return elementFound;
    }

public boolean waitForElementByXpathVisibility(String elementXpath)
    {
        boolean elementFound = false;
        try
        {
            int waitCount = 0;
            while (!elementFound && waitCount < ApplicationConfig.WaitTimeout())
            {
                try
                {
                    WebDriverWait wait = new WebDriverWait(Driver, 1);
                    if (wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(elementXpath))) != null)
                    {
                        if (wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(elementXpath))) != null)
                        {
                            elementFound = true;
                            break;
                        }
                    }
                }
                catch (Exception e)
                {
                    elementFound = false;
                }
                waitCount++;
            }

        }
        catch (Exception e)
        {
            Narrator.logError(" waiting for element by Xpath '" + elementXpath + "' - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
        }
        return elementFound;
    }
	
	


public boolean waitForElementByXpathVisibility(String elementXpath, Integer timeout)
    {
        boolean elementFound = false;
        try
        {
            int waitCount = 0;
            while (!elementFound && waitCount < timeout)
            {
                try
                {
                    WebDriverWait wait = new WebDriverWait(Driver, 1);
                    if (wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(elementXpath))) != null)
                    {
                        if (wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(elementXpath))) != null)
                        {
                            elementFound = true;
                            break;
                        }
                    }
                }
                catch (Exception e)
                {
                    elementFound = false;
                }
                waitCount++;
            }

        }
        catch (Exception e)
        {
            Narrator.logError(" waiting for element by Xpath '" + elementXpath + "' - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
        }
        return elementFound;
    }
	
	



public boolean stableClickElementByXpath(String elementXpath)
    {
        if (!SeleniumDriverInstance.waitForElementByXpath(elementXpath))
        {
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpathVisibility(elementXpath))
        {
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementToBeClickableByXpath(elementXpath))
        {
            return false;
        }

        if (ApplicationConfig.SelectedBrowser() == Enums.BrowserType.FireFox) 
        {
           if (!SeleniumDriverInstance.clickElementByXpath(elementXpath))
            {
                return false;
            }
        } 
        else
        {
            if (!SeleniumDriverInstance.clickElementByXpath_Actions(elementXpath))
            {
                return false;
            }
        }

        return true;
    }


 public boolean stableEnterTextByXpath(String elementXpath, String textToEnter)
    {
        if (!SeleniumDriverInstance.waitForElementByXpath(elementXpath))
        {
    //			error4 = "Failed to wait for the " + WaitedElement;
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpathVisibility(elementXpath))
        {
    //			error4 = "Failed to wait for the " + WaitedElement + " to be visble";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementToBeClickableByXpath(elementXpath))
        {
    //			error4 = "Failed to wait for " + WaitedElement + " to be clickable";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(elementXpath, textToEnter))
        {
    //			error4 = "Failed to enter text in the " + WaitedElement;
            return false;
        }

        return true;
    }
	


 public boolean stableSelectByTextFromDropDownListUsingXpath(String elementXpath, String valueToSelect)
    {
        if (!SeleniumDriverInstance.waitForElementByXpath(elementXpath))
        {
    //			error4 = "Failed to wait for the " + WaitedElement;
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpathVisibility(elementXpath))
        {
    //			error4 = "Failed to wait for the " + WaitedElement + " to be visble";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementToBeClickableByXpath(elementXpath))
        {
    //			error4 = "Failed to wait for the " + WaitedElement + " checkbox to be clickable";
            return false;
        }

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingXpath(elementXpath, valueToSelect))
        {
    //			error4 = "Failed to select " + valueToSelect + " from " + WaitedElement;
            return false;
        }

        return true;
    }
	
	
	
	
	
	
	 public boolean waitForElementByXpath(String elementXpath)
    {
        boolean elementFound = false;
        try
        {
            int waitCount = 0;
            while (!elementFound && waitCount < ApplicationConfig.WaitTimeout())
            {
                try
                {
                    WebDriverWait wait = new WebDriverWait(Driver, 1);
                    if (wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(elementXpath))) != null)
                    {
                        elementFound = true;
                        break;
                    }
                }
                catch (Exception e)
                {
                    elementFound = false;
                }
                waitCount++;
            }

        }
        catch (Exception e)
        {
            Narrator.logError(" waiting for element by Xpath '" + elementXpath + "' - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
        }
        return elementFound;
    }
	
	
	
	
	public boolean waitForElementToBeClickableByXpath(String elementXpath, Integer timeout)
    {
        boolean elementFound = false;
        try
        {
            int waitCount = 0;
            while (!elementFound && waitCount < timeout)
            {
                try
                {
                    WebDriverWait wait = new WebDriverWait(Driver, 1);
                    if (wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(elementXpath))) != null)
                    {
                        if (wait.until(ExpectedConditions.elementToBeClickable(By.xpath(elementXpath))) != null)
                        {
                            elementFound = true;
                            break;
                        }
                    }
                }
                catch (Exception e)
                {
                    elementFound = false;
                }
                waitCount++;
            }

        }
        catch (Exception e)
        {
            Narrator.logError(" waiting for element to be clickable by Xpath '" + elementXpath + "' - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
        }
        return elementFound;
    }
	
	
	
	
	public boolean clickElementByXpath_Actions(String elementXpath)
    {
        try
        {
            Narrator.logDebug(" Clicking element by Xpath : " + elementXpath);
            waitForElementByXpath(elementXpath);
            WebElement elementToClick = Driver.findElement(By.xpath(elementXpath));
            Actions builder = new Actions(Driver);
            builder.moveToElement(elementToClick).build().perform();
            builder.click(elementToClick).build().perform();

            return true;
        }
        catch (Exception e)
        {
            Narrator.logError(" Failed to click on element by Xpath - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }
    }
	
	
	
	
	public boolean clickElementByXpath(String elementXpath)
    {
        try
        {
            if (ApplicationConfig.SelectedBrowser() == Enums.BrowserType.IE) 
            {
                return clickElementByXpathActions(elementXpath);  
            }

            if (ApplicationConfig.SelectedBrowser() == Enums.BrowserType.FireFox) 
            {
               if (ApplicationConfig.SelectedBrowser() == Enums.BrowserType.FireFox)

                try
                {
                Actions action = new Actions(SeleniumDriverInstance.Driver);
                action.release();
                }
                catch(Exception ex)
                {

                }

                try 
                {
                    WebElement elementToClick1 = Driver.findElement(By.xpath(elementXpath));            
                    WebElement elementToClick2 = Driver.findElement(By.xpath(elementXpath+"//.."));
                    if((elementToClick1.getAttribute("class")!= null && elementToClick1.getAttribute("class").contains("requestTypeHeaders")) || (elementToClick2.getAttribute("class")!= null && elementToClick2.getAttribute("class").contains("requestTypeHeaders")))
                    {
                        return clickElementByXpathFireFox(elementXpath);
                    }
                    else if (elementToClick1.getAttribute("href")!= null ||(elementToClick2.getAttribute("onmouseout")!= null && elementToClick2.getAttribute("onmouseout").contains("ImageGreyMain();")))
                    {
                        return clickElementByXpathFireFox(elementXpath);  
                    }
                    else
                    {
                         List <WebElement> we = this.Driver.findElements(By.xpath(elementXpath));     


                        int ok_size=Driver.findElements(By.xpath(elementXpath)).size();

                        int j=0;
                        for (int i = 0; i < ok_size; i++) 
                        {
                            int x = we.get(i).getLocation().getX();

                            if (x!=0) 
                            {
                                we.get(i).click();
                                return true;
                            }
                            j++;
                        }
                        if (j == ok_size) 
                        {
                            return false;
                        }
                    }

                }
                catch (Exception e) 
                {
                    try
                    {
                        WebElement elementToClick2 = Driver.findElement(By.xpath(elementXpath+"//.."));

                        if (elementToClick2.getAttribute("onmouseout").contains("ImageGreyMain();"))
                        {
                            return clickElementByXpathFireFox(elementXpath);  
                        }
                    }
                    catch (Exception es) 
                    {



                        List <WebElement> we = this.Driver.findElements(By.xpath(elementXpath));     


                        int ok_size=Driver.findElements(By.xpath(elementXpath)).size();
                        int j=0;
                        for (int i = 0; i < ok_size; i++) 
                        {
                            int x = we.get(i).getLocation().getX();

                            if (x!=0) 
                            {
                                we.get(i).click();
                                return true;
                            }
                            j++;
                        }
                        if (j == ok_size) 
                        {
                            return false;
                        }

                    }

                }
            }
            else
            {
                Narrator.logDebug(" Clicking element by Xpath : " + elementXpath);
                waitForElementByXpath(elementXpath);
                waitForElementToBeClickableByXpath(elementXpath);
                WebDriverWait wait = new WebDriverWait(Driver, ApplicationConfig.WaitTimeout());
                wait.until(ExpectedConditions.elementToBeClickable(By.xpath(elementXpath)));
                WebElement elementToClick = Driver.findElement(By.xpath(elementXpath));
                elementToClick.click();
            }
            return true;

        }
        catch (Exception e)
        {
            Narrator.logError(" Failed to click on element by Xpath - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }

    }
	
	
	
	
	public boolean enterTextByXpath(String elementXpath, String textToEnter)
    {
        try
        {
            this.waitForElementByXpath(elementXpath);
            WebElement elementToTypeIn = Driver.findElement(By.xpath(elementXpath));
            this.pause(50);
            elementToTypeIn.clear();
            pause(200);
            Actions typeText = new Actions(Driver);
            typeText.moveToElement(elementToTypeIn);
            typeText.click(elementToTypeIn);
            typeText.sendKeys(elementToTypeIn, textToEnter);
            typeText.click(elementToTypeIn);
            typeText.perform();

            return true;
        }
        catch (Exception e)
        {
            Narrator.logError(" entering text - " + elementXpath + " - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }
    }
	
	
	
	
	
	public boolean selectByTextFromDropDownListUsingXpath(String elementXpath, String valueToSelect)
    {
        try
        {
            if (ApplicationConfig.SelectedBrowser() == Enums.BrowserType.IE) 
            {

            waitForElementByXpath(elementXpath);
            waitForElementToBeClickableByXpath(elementXpath);
            WebDriverWait wait = new WebDriverWait(Driver, ApplicationConfig.WaitTimeout());
            wait.until(ExpectedConditions.elementToBeClickable(By.xpath(elementXpath)));
            WebElement elementToClick = Driver.findElement(By.xpath(elementXpath));
            Actions builder = new Actions(Driver);
            builder.moveToElement(elementToClick).build().perform();
            builder.click(elementToClick).build().perform();
                        List<WebElement> options = elementToClick.findElements(By.tagName("option"));
            for (WebElement option : options)
            {
                String optTxt = option.getText();
                if (optTxt.equals(valueToSelect))
                {
            builder.moveToElement(option).build().perform();
            builder.click(option).build().perform();
                    //Actions action = new Actions(SeleniumDriverInstance.Driver);
                    //action.sendKeys(Keys.ENTER).build().perform();
                    //option.click();

                    Narrator.logError("[Info] Selected partial text '" + valueToSelect + "' from element '" + elementXpath + "'");
                    return true;
                }
            }

            }
            else{
            waitForElementByXpath(elementXpath);
            Select dropDownList = new Select(Driver.findElement(By.xpath(elementXpath)));

            dropDownList.selectByVisibleText(valueToSelect);
    //            dropDownList.selectByValue(valueToSelect);

    //                Actions action = new Actions(SeleniumDriverInstance.Driver);
    //                action.sendKeys(Keys.ENTER).build().perform();
            }
            return true;

        }
        catch (Exception e)
        {
            Narrator.logError("selecting from dropdownlist by text using xpath - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }
    }
	
	public boolean clickElementByXpathFireFox(String elementXpath)
    {
         if (ApplicationConfig.SelectedBrowser() == Enums.BrowserType.FireFox)
        {
            try
            {
            Actions action = new Actions(SeleniumDriverInstance.Driver);
            action.release().build().perform();
            }
            catch(Exception ex)
            {

            }
        }
        try
        {

            waitForElementByXpath(elementXpath);
            waitForElementToBeClickableByXpath(elementXpath);
            WebDriverWait wait = new WebDriverWait(Driver, ApplicationConfig.WaitTimeout());
            wait.until(ExpectedConditions.elementToBeClickable(By.xpath(elementXpath)));
    //            WebElement elementToClick = Driver.findElement(By.xpath(elementXpath));

            WebElement we = this.Driver.findElement(By.xpath(elementXpath));

            JavascriptExecutor executor = (JavascriptExecutor) Driver;
            executor.executeScript("arguments[0].click();", we);

    //           JavascriptExecutor js = (JavascriptExecutor) Driver;
    //            js.executeScript("var evt = document.createEvent('MouseEvents');" + "evt.initMouseEvent('click',true, true, window, 0, 0, 0, 0, 0, false, false, false, false, 0,null);" + "arguments[0].dispatchEvent(evt);", elementToClick);
            return true;
        }
        catch (Exception e)
        {
            Narrator.logError(" Failed to click on element by Xpath - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }
    }
        
        public boolean waitForElementByClassName(String elementClassName)
    {
        boolean elementFound = false;
        try
        {
            int waitCount = 0;
            while (!elementFound && waitCount < ApplicationConfig.WaitTimeout())
            {
                try
                {
                    WebDriverWait wait = new WebDriverWait(Driver, 1);
                    if (wait.until(ExpectedConditions.presenceOfElementLocated(By.className(elementClassName))) != null)
                    {
                        elementFound = true;
                        break;
                    }
                }
                catch (Exception e)
                {
                    elementFound = false;
                }
                waitCount++;
            }
        }
        catch (Exception e)
        {
            Narrator.logError(" waiting for element by class name : '" + elementClassName + "' - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
        }
        return elementFound;
    }
        
         public boolean clickElementbyClassName(String elementClassName)
    {
        try
        {
            this.waitForElementByClassName(elementClassName);
            WebElement elementToClick = Driver.findElement(By.className(elementClassName));
            elementToClick.click();

            return true;
        }
        catch (Exception e)
        {
            Narrator.logError(" clicking element by class name  - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }
    }
         
         public boolean enterTextByClassName(String elementClassName, String textToEnter)
    {
        try
        {
            this.waitForElementByClassName(elementClassName);
            WebElement elementToTypeIn = Driver.findElement(By.className(elementClassName));
            elementToTypeIn.clear();
            elementToTypeIn.sendKeys(textToEnter);

            return true;
        }
        catch (Exception e)
        {
            Narrator.logError(" entering text by class name  - " + elementClassName + "  : " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }
    }
         
         public boolean waitForElementNoLongerPresentByClass(String elementClass, Integer timeout)
    {
        boolean elementFound = true;
        try
        {
            int waitCount = 0;
            while (elementFound && waitCount < timeout)
            {
                try
                {
                    Driver.findElement(By.className(elementClass));
                    elementFound = true;
                }
                catch (Exception e)
                {
                    this.DriverExceptionDetail = e.getMessage();
                    elementFound = false;
                    break;
                }
                Thread.sleep(500);
                waitCount++;
            }
        }
        catch (Exception e)
        {
            Narrator.logError(" waiting for element to be no longer present by Class  - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
        }
        if (elementFound)
        {
            return false;
        }
        else
        {
            return true;
        }
    }
         public void copyKeys()
    {
        try
        {
            Actions action = new Actions(Driver);
            action.sendKeys(Keys.CONTROL, "c");
            action.perform();
        }
        catch (Exception e)
        {
            this.DriverExceptionDetail = e.getMessage();
            Narrator.logError(" Failed to send keypress to element - Contoll + C");

        }
    }
         
         public void shutDown()
    {
        retrievedTestValues = null;
        try
        {

            Driver.quit();

        }
        catch (Exception e)
        {
            Narrator.logError(" shutting down driver - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
        }
        _isDriverRunning = false;
    }
        
        public void takeScreenShot(String screenShotDescription, boolean isError)
    {
        String imageFilePathString = "";

        try
        {
            StringBuilder imageFilePathBuilder = new StringBuilder();
            // add date time folder and test case id folder
            imageFilePathBuilder.append(this.reportDirectory + "\\" + testCaseId + "\\");

            if (isError)
            {
                imageFilePathBuilder.append("FAILED_");
            }
            else
            {
                imageFilePathBuilder.append("PASSED_");
            }

            imageFilePathBuilder.append(testCaseId + "_");

            imageFilePathBuilder.append(screenShotDescription + ".png");

            //imageFilePathBuilder.append(this.generateDateTimeString() + ".png");
            imageFilePathString = imageFilePathBuilder.toString();

            setScreenshotPath(imageFilePathString);

            File screenShot = ((TakesScreenshot) Driver).getScreenshotAs(OutputType.FILE);
            FileUtils.copyFile(screenShot, new File(imageFilePathString.toString()));
        }
        catch (Exception e)
        {
            Narrator.logError(" could not take screenshot - " + imageFilePathString.toString() + " - " + e.getMessage());
        }
    }

    }
