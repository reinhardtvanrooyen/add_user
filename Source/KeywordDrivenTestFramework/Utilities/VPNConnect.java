/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Utilities;

import KeywordDrivenTestFramework.Core.BaseClass;
import java.net.InetAddress;

/**
 *
 * @author szeuch
 */

public class VPNConnect extends BaseClass {
   
    public boolean connectToVPN()
    {
        String username = null;
        String password = null;
        InetAddress addr;
        try 
        {
            addr = InetAddress.getLocalHost();
            String hostname = addr.getHostName();
            
            //Jason
            if(hostname.equalsIgnoreCase("DVT-INT17"))
            {
                username = "dvt1";
                password = "fuxAhefab5qA7usW";
            }
            else if(hostname.equalsIgnoreCase("dvt-gtc27"))
            {
                username = "dvt1";
                password = "fuxAhefab5qA7usW";
            }
            //Stefan
            else if (hostname.equalsIgnoreCase("dvt-sqa12")) 
            {
                username = "dvt2";
                password = "fusuxAfRE7rusWuz";
            }
            //Trent
            else if (hostname.equalsIgnoreCase("DVT-SQA43")) 
            {
                username = "dvt3";
                password = "brumepruq58rEw3P";
            }
            //Siya
            else if(hostname.equalsIgnoreCase("dvt-sqa61"))
            {
                username = "dvt4";
                password = "sPu4UwresP6tH8Sw";
            }
            //Beni
            else if(hostname.equalsIgnoreCase("DVT-sqa53"))
            {
                username = "dvt5";
                password = "ha7ruspU6pup3uFR";
            }
            //Xolelwa
            else if(hostname.equalsIgnoreCase("DVT-INT11"))
            {
                username = "dvt6";
                password = "bUQap6UwRenav5ja";
            }
            //Garreth
            else if(hostname.equalsIgnoreCase("DVT-SQA55"))//DVT-Jade-nb
            {
                username = "dvt7";
                password = "FeVAtafr4FRUdrat";
            }
            //Achmat
            else if(hostname.equalsIgnoreCase("DVT-INT15"))
            {
                username = "dvt8";
                password = "cheswuyedReyama5";
            }
            //Nelson
            else if(hostname.equalsIgnoreCase("DVT-sqa54"))
            {
                username = "dvt9";
                password = "J5yEXugaw2ha6Wac";
            }
            //Chris
            else if(hostname.equalsIgnoreCase("dvt-dev24"))
            {
                username = "dvt10";
                password = "TruPrustuhEhere8";
            }
            else if(hostname.equalsIgnoreCase("DVT-SQA29"))
            {
                username = "dvt11";
                password = "xUdrUHuStespEM35";
            }
            else if(hostname.equalsIgnoreCase("DVT-INT12"))
            {
                username = "dvt12";
                password = "veYurubR44u3RERU";
            }
            //Fab
            else if(hostname.equalsIgnoreCase("DVT-INT10"))
            {
                username = "dvt13";
                password = "Br57hebraquYAdru";
            }
            //Jason Lee Young
            else if(hostname.equalsIgnoreCase("DVT-INT14"))
            {
                username = "dvt14";
                password = "duzeve3ehapruxAd";
            }
            //Witness
            else if(hostname.equalsIgnoreCase("DVT-SQA42"))
            {
                username = "dvt15";
                password = "Ph7chuTHEHuDraCu";
            }
            //Execution Box
            else if(hostname.equalsIgnoreCase(""))
            {
                username = "dvt16";
                password = "racukAH8watHup6d";
            }
            //Chante
            else if(hostname.equalsIgnoreCase("DVT-INT03"))
            {
                username = "dvt17";
                password = "JEp2sWuWa2haWesa";
            }
            else
            {
                return true;
            }
            System.out.println("");
            System.out.println("[INFO] Connected to VPN '"+username+"' on machine '"+hostname+"'");
            String connect = "rasdial \"Sintrex VPN Connection\" \""+username+"\" \""+password+"\"";
            Runtime.getRuntime().exec(connect);
            Thread.sleep(5000);
        } 
        catch (Exception ex)
        {
            System.out.println("[ERROR] - " + ex.getMessage());
            return false;
        }
        return true;
    }
    
    public boolean disconnectVPN()
    {
        try
        {
            String disconnect = "rasdial \"Sintrex VPN Connection\" /disconnect";
            Runtime.getRuntime().exec(disconnect);
            Thread.sleep(2000);
        }
        catch(Exception ex)
        {
            System.out.println("[ERROR] - " + ex.getMessage());
        }
        
        return true;
    }
    public boolean DriverTaskkill()
    { 
        try
        {
            String taskkillchrome = "taskkill /f /im chromedriver.exe";
            Runtime.getRuntime().exec(taskkillchrome);

            String taskkillfox = "taskkill /f /im firefoxdriver.exe";
            Runtime.getRuntime().exec(taskkillfox);
            
            String taskkillIE = "taskkill /f /im ieserverdriver.exe";
            Runtime.getRuntime().exec(taskkillIE);
        }
        catch(Exception ex)
        {
            System.out.println("[ERROR] - " + ex.getMessage());
        }
        return true;
    }
}
