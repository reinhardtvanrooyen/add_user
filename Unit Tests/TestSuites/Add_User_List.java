/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TestSuites;

import KeywordDrivenTestFramework.Entities.Enums;
import KeywordDrivenTestFramework.Reporting.Narrator;
import KeywordDrivenTestFramework.Testing.TestMarshall;
import KeywordDrivenTestFramework.Utilities.ApplicationConfig;
import java.io.FileNotFoundException;
import org.junit.Test;

/**
 *
 * @author Reinhardt van Rooyen
 */
public class Add_User_List 
{
    static TestMarshall instance; 

    public Add_User_List() 
    {
        ApplicationConfig appConfig = new ApplicationConfig();
        TestMarshall.currentEnvironment = Enums.Environment.AddUserListURL;
    }
    
    @Test
    public void RunAdd_User_ListChrome() throws FileNotFoundException
    {
        Narrator.logDebug("Add_Users_List - Test Pack");
        instance = new TestMarshall("TestPacks\\Add_Users_List.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
   
}